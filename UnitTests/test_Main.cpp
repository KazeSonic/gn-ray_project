#include <gtest/gtest.h>
#include <GN-Ray/Scene.hpp>
#include <GN-Ray/Window/Window.hpp>
#include <GN-Ray/RayTracing.hpp>
#include <GN-Ray/Camera/Camera.hpp>
#include <GN-Ray/Graphics.hpp>
#include <GN-Ray/Lights.hpp>

#include <chrono>
#include <iostream>

TEST(Main_main, run_lib)
{
    gn::Window window(800, 600, "pouet");

    gn::scene::SceneGraph scene;
    
    ::gn::scene::Node rbtSphereNode;
    rbtSphereNode.set_rbt(gn::math::Rbt(gn::math::Vector3(0, 0, -10)));
    ::gn::scene::ShapeNode node;
    rbtSphereNode.add_child(&node);
    gn::graphics::SphereShape sphere;
    sphere.set_radius(3.0f);
    node.set_shape(&sphere);
    
    scene.get_root().add_child(&rbtSphereNode);

    // LIGHT
    ::gn::lights::Light light;
    ::gn::scene::LightNode lightNode;
    lightNode.set_light(&light);
    ::gn::scene::Node rbtLightNode;
    rbtLightNode.set_rbt(::gn::math::Rbt(::gn::math::Vector3(10, 0, -10)));
    rbtLightNode.add_child(&lightNode);
    scene.get_root().add_child(&rbtLightNode);
    // END LIGHT

    gn::Camera camera;

    gn::ray::RayTracer rayTracer;

    std::chrono::high_resolution_clock::time_point start;
    std::chrono::high_resolution_clock::time_point end;
    while(window.is_open())
    {
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        {
            camera.get_rbt().get_translation()[0] -= 0.1;
        }
        else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        {
            camera.get_rbt().get_translation()[0] += 0.1;
        }

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        {
            camera.get_rbt().get_translation()[1] += 0.1;
        }
        else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        {
            camera.get_rbt().get_translation()[1] -= 0.1;
        }

        if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            rbtLightNode.set_rbt(rbtSphereNode.get_rbt() * ::gn::math::Rbt(::gn::math::quaternion::make_y_rotation(10)) * rbtSphereNode.get_rbt().get_inverse() * rbtLightNode.get_rbt() );
        }


        sf::Event event;
        while(window.get_SFMLWindow().pollEvent(event))
        {
            if(event.type == sf::Event::Closed)
            {
                window.get_SFMLWindow().close();
                break;
            }
        }

        start = std::chrono::high_resolution_clock::now();
        rayTracer.ray_trace(window, camera, scene);
        end = std::chrono::high_resolution_clock::now();
        std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(end - start ).count() << " ms " << std::endl;

        window.display();
    }
}