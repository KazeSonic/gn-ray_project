#include <gtest/gtest.h>
#include <cmath>

#include <GN-Ray/Math/Quaternion.hpp>
#include <GN-Ray/Math/Vector.hpp>
#include <GN-Ray/Math/Matrix4.hpp>

TEST(Math_Quaternion, constructor)
{
    gn::math::Quaternion quat;
    for(int i = 0; i < 4; ++i)
    {
        if(i == 0) EXPECT_DOUBLE_EQ(1.0, quat[i]);
        else EXPECT_DOUBLE_EQ(0.0, quat[i]);
    }

    gn::math::Vector4 vec;
    for(int i = 0; i < 4; ++i) vec[i] = i;

    quat = gn::math::Quaternion(vec);
    for(int i = 0; i < 4; ++i) EXPECT_DOUBLE_EQ(vec[i], quat[i]);

    vec += 1;
    quat = gn::math::Quaternion(vec[0], gn::math::Vector3(vec[1], vec[2], vec[3]));
    for(int i = 0; i < 4; ++i) EXPECT_DOUBLE_EQ(vec[i], quat[i]);

    vec += 1;
    quat = gn::math::Quaternion(vec[0], vec[1], vec[2], vec[3]);
    for(int i = 0; i < 4; ++i) EXPECT_DOUBLE_EQ(vec[i], quat[i]);
}

TEST(Math_Quaternion, arithmetic_operator)
{
    gn::math::Quaternion q1;
    gn::math::Quaternion q2;
    gn::math::Quaternion result;
    gn::math::Quaternion expected_result;

    // -= and -
    {
        q1 = gn::math::Quaternion(3, -2, 0, 7);
        q2 = gn::math::Quaternion(1, 7, 5, 6);
        expected_result = gn::math::Quaternion(2, -9, -5, 1);
        result = q1;
        EXPECT_NO_THROW(result -= q2);

        EXPECT_EQ(expected_result, result);

        EXPECT_NO_THROW(result = q1 - q2);
        EXPECT_EQ(expected_result, result);

        expected_result = gn::math::Quaternion(-3, 2, 0, -7);
        EXPECT_NO_THROW(result = -q1);
        EXPECT_EQ(expected_result, result);
    }

    // += and +
    {
        q1 = gn::math::Quaternion(3, -2, 0, 7);
        q2 = gn::math::Quaternion(1, 7, 5, 6);
        expected_result = gn::math::Quaternion(4, 5, 5, 13);

        result = q1;
        EXPECT_NO_THROW(result += q2);
        EXPECT_EQ(expected_result, result);

        EXPECT_NO_THROW(result = q1 + q2);
        EXPECT_EQ(expected_result, result);
    }

    // *= and *
    {
        q1 = gn::math::Quaternion(3, -2, 0, 7);
        q2 = gn::math::Quaternion(1, 7, 5, 6);
        double value = 2.0;

        expected_result = gn::math::Quaternion(6, -4, 0, 14);
        result = q1;
        EXPECT_NO_THROW(result *= value);
        EXPECT_EQ(expected_result, result);

        expected_result = gn::math::Quaternion(-25, -16, 76, 15);
        EXPECT_NO_THROW(result = q1 * q2);
        EXPECT_EQ(expected_result, result);

        expected_result = gn::math::Quaternion(6, -4, 0, 14);
        EXPECT_NO_THROW(result = q1 * value);
        EXPECT_EQ(expected_result, result);

        /////////////////
        //CHECK THIS
        /////////////////
        gn::math::Vector4 vec(1, 1, 1, 1);
        q1 = gn::math::Quaternion(1, 0, 1, 0);
        gn::math::Vector4 expected_vec_result(1, 1, -1, 1);
        EXPECT_NO_THROW(result = q1 * vec);
        EXPECT_EQ(expected_result, result);
    }

    // /= and /
    {
        q1 = gn::math::Quaternion(3, -2, 0, 7);
        double value = 2.0;

        expected_result = gn::math::Quaternion(1.5, -1, 0, 3.5);
        result = q1;
        EXPECT_NO_THROW(result /= value);
        EXPECT_EQ(expected_result, result);

        EXPECT_NO_THROW(result = q1 / value);
        EXPECT_EQ(expected_result, result);
    }
}

TEST(Math_Quaternion, get_inverse)
{
    gn::math::Quaternion q1(3, -2, 0, 7);
    gn::math::Quaternion expected_result(0.048387096774194, 
                                         0.032258064516129,
                                         0,
                                         -0.112903225806452);

    gn::math::Quaternion result;
    EXPECT_NO_THROW(result = q1.get_inverse());
    EXPECT_EQ(expected_result, result);
}

TEST(Math_Quaternion, get_norm)
{
    gn::math::Quaternion q1(3, -2, 0, 7);
    double expected_result = 62;

    double result;
    EXPECT_NO_THROW(result = q1.get_norm());
    EXPECT_EQ(expected_result, result);
}

TEST(Math_Quaternion, normalize)
{
    gn::math::Quaternion q1(3, -2, 0, 7);
    double norm = q1.get_norm();
    gn::math::Quaternion expected_result(3 / std::sqrt(norm), -2 / std::sqrt(norm), 0 / std::sqrt(norm), 7 / std::sqrt(norm));

    gn::math::Quaternion result;
    EXPECT_NO_THROW(result = q1.get_normalize_quaternion());
    EXPECT_EQ(expected_result, result);

    EXPECT_NO_THROW(q1.normalize());
    EXPECT_EQ(expected_result, q1);
}

TEST(Math_Quaternion, dot_product)
{
    gn::math::Quaternion q1(3, -2, 0, 7);
    gn::math::Quaternion q2(1, 7, 5, 6);
    double expected_result = 31;

    double result;
    EXPECT_NO_THROW(result = q1.dot_product(q2));
    EXPECT_EQ(expected_result, result);
}

TEST(Math_Quaternion, to_matrix)
{
    gn::math::Quaternion q1(3, -2, 0, 7);
    gn::math::Matrix4 expected_result;
    expected_result(0, 0) = -0.580645161290323;
    expected_result(0, 1) = -0.677419354838710;
    expected_result(0, 2) = -0.451612903225806;

    expected_result(1, 0) = 0.677419354838710;
    expected_result(1, 1) = -0.709677419354839;
    expected_result(1, 2) = 0.193548387096774;

    expected_result(2, 0) = -0.451612903225806;
    expected_result(2, 1) = -0.193548387096774;
    expected_result(2, 2) = 0.870967741935484;


    gn::math::Matrix4 result;
    EXPECT_NO_THROW(result = q1.to_matrix());
    EXPECT_EQ(expected_result, result);
}