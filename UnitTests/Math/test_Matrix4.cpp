#include <gtest/gtest.h>
#include <GN-Ray/Math/Matrix4.hpp>

namespace test_Matrix4
{
    gn::math::Matrix4 get_test_result_matrix()
    {
        gn::math::Matrix4 matrix;
        matrix[0] = 90;   matrix[1] = 100;  matrix[2] = 110;  matrix[3] = 120;
        matrix[4] = 202;  matrix[5] = 228;  matrix[6] = 254;  matrix[7] = 280;
        matrix[8] = 314;  matrix[9] = 356;  matrix[10] = 398; matrix[11] = 440;
        matrix[12] = 426; matrix[13] = 484; matrix[14] = 542; matrix[15] = 600;
        return matrix;
    }
    
    gn::math::Matrix4 get_determinant_test_matrix()
    {
        gn::math::Matrix4 matrix;
        matrix[0] = 1;  matrix[1] = -1; matrix[2] = 2;   matrix[3] = 3;
        matrix[4] = 1;  matrix[5] = 5;  matrix[6] = 6;   matrix[7] = 3;
        matrix[8] = 9;  matrix[9] = 4;  matrix[10] = -2; matrix[11] = 8;
        matrix[12] = 0; matrix[13] = 0; matrix[14] = 0;  matrix[15] = 1;
        return matrix;
    }

    gn::math::Matrix4 get_inverse_test_matrix()
    {
        gn::math::Matrix4 matrix;
        matrix[0] = 0.1977;  matrix[1] = -0.0349; matrix[2] = 0.0930;   matrix[3] = -1.2326;
        matrix[4] = -0.3256;  matrix[5] = 0.1163;  matrix[6] = 0.0233;   matrix[7] = 0.4419;
        matrix[8] = 0.2384;  matrix[9] = 0.0756;  matrix[10] = -0.0349; matrix[11] = -0.6628;
        matrix[12] = 0; matrix[13] = 0; matrix[14] = 0;  matrix[15] = 1;
        return matrix;
    }
}


TEST(Math_Matrix, constructor)
{
    gn::math::Matrix4 matrix;

    //should be identity matrix
    for(int i = 0; i < 16; i++)
    {
        if(i == 0 || i == 5 || i == 10 || i == 15)
            EXPECT_DOUBLE_EQ(1.0, matrix[i]);
        else
            EXPECT_DOUBLE_EQ(0.0, matrix[i]);
    }

    matrix = gn::math::Matrix4(2.0);

    for(int i = 0; i < 16; i++)
    {
        EXPECT_DOUBLE_EQ(2.0, matrix[i]);
    }
}

TEST(Math_Matrix, comparison_operator)
{
    gn::math::Matrix4 matrix1;
    gn::math::Matrix4 matrix2(1.0);

    EXPECT_FALSE(matrix1 == matrix2);
    EXPECT_TRUE(matrix1 != matrix2);
    matrix1 = matrix2;
    EXPECT_TRUE(matrix1 == matrix2);
    EXPECT_FALSE(matrix1 != matrix2);
}

TEST(Math_Matrix, arithmetic_operator)
{
    gn::math::Matrix4 m1(0), m2;
    gn::math::Matrix4 result, expected_result;
    // | 1  2  3  4 | * | 1  2  3  4 | = | 90  100 110 120|
    // | 5  6  7  8 |   | 5  6  7  8 |   | 202 228 254 280|
    // | 9  10 11 12|   | 9  10 11 12|   | 314 356 398 440|
    // | 13 14 15 16|   | 13 14 15 16|   | 426 484 542 600|
    for(int i = 1; i <= 16; ++i) m1[i - 1] = i;

    // *= and *
    {
        //m *= m
        expected_result = test_Matrix4::get_test_result_matrix();
        m2 = m1;
        result = m1;
        EXPECT_NO_THROW(result *= m1);
        EXPECT_EQ(expected_result, result);

        //m *= double
        result = m1;
        EXPECT_NO_THROW(result *= 2);
        for(int i = 0; i < 16; ++i)
        {
            EXPECT_DOUBLE_EQ(m1[i] * 2, result[i]);
        }

        //m * m
        m2 = m1;
        EXPECT_NO_THROW(result = m1 * m2);
        EXPECT_EQ(expected_result, result);

        //m * vec
        gn::math::Vector4 vec(1, 2, 3, 4);
        gn::math::Vector4 expected_result_vec(30, 70, 110, 150);
        gn::math::Vector4 result_vec;

        EXPECT_NO_THROW(result_vec = m1 * vec);
        EXPECT_EQ(expected_result_vec, result_vec);

        //m * double
        result = m1;
        EXPECT_NO_THROW(result = m1 * 2);
        for(int i = 0; i < 16; ++i)
        {
            EXPECT_DOUBLE_EQ(m1[i] * 2, result[i]);
        }
    }

    // -= and -
    {
        m2 = m1;
        //m -= m
        expected_result = gn::math::Matrix4(0);
        result = m1;
        EXPECT_NO_THROW(result -= m1);
        EXPECT_EQ(expected_result, result);

        //m - m
        EXPECT_NO_THROW(result = m1 - m2);
        EXPECT_EQ(expected_result, result);

        //- m
        EXPECT_NO_THROW(result = -m1);
        for(int i = 0; i < 16; ++i)
        {
            EXPECT_DOUBLE_EQ(m1[i] * -1, result[i]);
        }
    }

    // += and +
    {
        m2 = m1;
        //m += m
        expected_result = m1 * 2;
        result = m1;
        EXPECT_NO_THROW(result += m1);
        EXPECT_EQ(expected_result, result);

        //m + m
        EXPECT_NO_THROW(result = m1 + m2);
        EXPECT_EQ(expected_result, result);
    }
}

TEST(Math_Matrix, is_affine)
{
    gn::math::Matrix4 matrix; //< identity matrix

    EXPECT_TRUE(matrix.is_affine());
    matrix = test_Matrix4::get_test_result_matrix();
    EXPECT_FALSE(matrix.is_affine());
}

TEST(Math_Matrix, get_determinant)
{
    gn::math::Matrix4 matrix = test_Matrix4::get_determinant_test_matrix();
    double expected_result = -172.0;
    double result;
    EXPECT_NO_THROW(result = matrix.get_determinant());
    EXPECT_DOUBLE_EQ(expected_result, result);
}

TEST(Math_Matrix, get_inverse)
{
    gn::math::Matrix4 matrix = test_Matrix4::get_determinant_test_matrix();
    gn::math::Matrix4 result;
    EXPECT_NO_THROW(result = matrix.get_inverse_matrix());
    EXPECT_TRUE(result.is_affine());
    //EXPECT_EQ(test_Matrix4::get_inverse_test_matrix(), result);
    gn::math::Matrix4 test = matrix * result;
    EXPECT_EQ(gn::math::Matrix4(), matrix * result);
    test = result * matrix;
    EXPECT_EQ(gn::math::Matrix4(), result * matrix);
}

TEST(Math_Matrix, get_transpose)
{
    gn::math::Matrix4 matrix;
    for(int i = 1; i <= 16; ++i) matrix[i - 1] = i;

    gn::math::Matrix4 result, expected_result;
    for(int i = 0; i < 4; ++i) 
        for(int j = 0; j < 4; ++j) 
            expected_result(j, i) = (i * 4) + j + 1;

    EXPECT_NO_THROW(result = matrix.get_transpose());
    EXPECT_EQ(expected_result, result);
}


TEST(Math_Matrix, get_translation_linear_factor)
{
    gn::math::Matrix4 matrix;
    for(int i = 1; i <= 16; ++i) matrix[i - 1] = i;

    gn::math::Matrix4 result, expected_result;
    //translation factor
    expected_result[3]  = matrix[3];
    expected_result[7]  = matrix[7];
    expected_result[11] = matrix[11];
    EXPECT_NO_THROW(result = matrix.get_translation_factor());
    EXPECT_EQ(expected_result, result);
    
    //linear factor
    expected_result = matrix;
    expected_result[3] = 0;  expected_result[7] = 0;  expected_result[11] = 0;
    expected_result[12] = 0; expected_result[13] = 0; expected_result[14] = 0; expected_result[15] = 1;
    EXPECT_NO_THROW(result = matrix.get_linear_factor());
    EXPECT_EQ(expected_result, result);
}



