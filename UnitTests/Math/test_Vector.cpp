#include <gtest/gtest.h>
#include <GN-Ray/Math/Vector.hpp>

TEST(Math_Vector, constructor)
{
    //Vector 2
    {
        gn::math::Vector2 vec2;

        for(int i = 0; i < 2; ++i) EXPECT_DOUBLE_EQ(0.0, vec2[i]);
        vec2 = gn::math::Vector2(1.0);
        for(int i = 0; i < 2; ++i) EXPECT_DOUBLE_EQ(1.0, vec2[i]);
        vec2 = gn::math::Vector2(1.0, 2.0);
        for(int i = 0; i < 2; ++i) EXPECT_DOUBLE_EQ(i + 1.0, vec2[i]);
    }

    //Vector 3
    {
        gn::math::Vector3 vec3;

        for(int i = 0; i < 3; ++i) EXPECT_DOUBLE_EQ(0.0, vec3[i]);
        vec3 = gn::math::Vector3(1.0);
        for(int i = 0; i < 3; ++i) EXPECT_DOUBLE_EQ(1.0, vec3[i]);
        vec3 = gn::math::Vector3(1.0, 2.0, 3.0);
        for(int i = 0; i < 3; ++i) EXPECT_DOUBLE_EQ(i + 1.0, vec3[i]);
    }

    //Vector 4
    {
        gn::math::Vector4 vec4;

        for(int i = 0; i < 4; ++i) EXPECT_DOUBLE_EQ(0.0, vec4[i]);
        vec4 = gn::math::Vector4(1.0);
        for(int i = 0; i < 4; ++i) EXPECT_DOUBLE_EQ(1.0, vec4[i]);
        vec4 = gn::math::Vector4(1.0, 2.0, 3.0, 4.0);
        for(int i = 0; i < 4; ++i) EXPECT_DOUBLE_EQ(i + 1.0, vec4[i]);
    }

    //copy with different size
    {
        gn::math::Vector3 vec3(1, 2, 3);
        gn::math::Vector4 vec4(vec3, 4);

        for(int i = 0; i < 4; ++i) EXPECT_DOUBLE_EQ(i + 1, vec4[i]);

        vec4 = gn::math::Vector4(3, 2, 1, 0);
        vec3 = gn::math::Vector3(vec4);
        for(int i = 0; i < 3; ++i) EXPECT_DOUBLE_EQ(3 - i, vec3[i]);
    }

}

TEST(Math_Vector, comparison_operator)
{
    gn::math::Vector3 vec1(3, -2, 0);
    gn::math::Vector3 vec2(1, 7, 5);

    EXPECT_FALSE(vec1 == vec2);
    EXPECT_TRUE(vec1 != vec2);
    vec1 = vec2;
    EXPECT_TRUE(vec1 == vec2);
    EXPECT_FALSE(vec1 != vec2);
}

TEST(Math_Vector, arithmetic_operator)
{
    gn::math::Vector3 vec1;
    gn::math::Vector3 vec2;
    gn::math::Vector3 result;
    gn::math::Vector3 expected_result;

    // -= and -
    {
        vec1 = gn::math::Vector3(3, -2, 0);
        vec2 = gn::math::Vector3(1, 7, 5);
        expected_result = gn::math::Vector3(2, -9, -5);
        result = vec1;
        EXPECT_NO_THROW(result -= vec2);

        EXPECT_EQ(expected_result, result);

        EXPECT_NO_THROW(result = vec1 - vec2);
        EXPECT_EQ(expected_result, result);

        expected_result = gn::math::Vector3(-3, 2, 0);
        EXPECT_NO_THROW(result = -vec1);
        EXPECT_EQ(expected_result, result);
    }

    // += and +
    {
        vec1 = gn::math::Vector3(3, -2, 0);
        vec2 = gn::math::Vector3(1, 7, 5);
        expected_result = gn::math::Vector3(4, 5, 5);

        result = vec1;
        EXPECT_NO_THROW(result += vec2);
        EXPECT_EQ(expected_result, result);

        EXPECT_NO_THROW(result = vec1 + vec2);
        EXPECT_EQ(expected_result, result);
    }

    // *= and *
    {
        vec1 = gn::math::Vector3(3, -2, 0);
        double value = 2.0;

        expected_result = gn::math::Vector3(6, -4, 0);
        result = vec1;
        EXPECT_NO_THROW(result *= value);
        EXPECT_EQ(expected_result, result);

        EXPECT_NO_THROW(result = vec1 * value);
        EXPECT_EQ(expected_result, result);
    }

    // /= and /
    {
        vec1 = gn::math::Vector3(3, -2, 0);
        double value = 2.0;

        expected_result = gn::math::Vector3(1.5, -1, 0);
        result = vec1;
        EXPECT_NO_THROW(result /= value);
        EXPECT_EQ(expected_result, result);

        EXPECT_NO_THROW(result = vec1 / value);
        EXPECT_EQ(expected_result, result);
    }
}


TEST(Math_Vector, get_norm)
{
    gn::math::Vector3 vec(3, 0, 4);
    double expected_result = 5;

    double result;
    EXPECT_NO_THROW(result = vec.get_norm());
    EXPECT_EQ(expected_result, result);
}

TEST(Math_Vector, normalize)
{
    gn::math::Vector3 vec(3, 0, 4);
    gn::math::Vector3 expected_result(3.0/5.0, 0.0, 4.0/5.0);

    gn::math::Vector3 result;
    EXPECT_NO_THROW(result = vec.get_normalize_vector());
    EXPECT_EQ(expected_result, result);

    EXPECT_NO_THROW(vec.normalize());
    EXPECT_EQ(expected_result, vec);
}

TEST(Math_Vector, dot_product)
{
    gn::math::Vector3 vec1(3, -2, 0);
    gn::math::Vector3 vec2(1, 7, 5);
    double expected_result = -11;

    double result;
    EXPECT_NO_THROW(result = vec1.dot_product(vec2));
    EXPECT_EQ(expected_result, result);
}

TEST(Math_Vector, cross_product)
{
    gn::math::Vector3 vec1(3, -2, 0);
    gn::math::Vector3 vec2(1, 7, 5);
    gn::math::Vector3 expected_result(-10, -15, 23);

    gn::math::Vector3 result;
    EXPECT_NO_THROW(result = vec1.cross_product(vec2));
    EXPECT_EQ(expected_result, result);
}