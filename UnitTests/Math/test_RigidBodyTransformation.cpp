#include <gtest/gtest.h>
#include <GN-Ray/Math/RigidBodyTransformation.hpp>

namespace Math_RigidBodyTransformation
{
    gn::math::Matrix4 get_matrix_result_test_multiply()
    {
        gn::math::Matrix4 expected_result;
        expected_result(1, 1) = 0; 
        expected_result(2, 1) = 1;
        expected_result(1, 2) = -1;
        expected_result(2, 2) = 0;
        expected_result(0, 3) = 1;
        expected_result(1, 3) = 2;
        expected_result(2, 3) = 3;

        return expected_result;
    }
}


TEST(Math_RigidBodyTransformation, constructor)
{
    {
        gn::math::Rbt rbt;
        EXPECT_EQ(gn::math::Vector3(), rbt.get_translation());
        EXPECT_EQ(gn::math::Quaternion(), rbt.get_rotation());
    }

    {
        gn::math::Vector3 vec(1, 2, 3);
        gn::math::Quaternion quat(1, 1, 1, 0);
        gn::math::Rbt rbt(vec, quat);
        EXPECT_EQ(vec, rbt.get_translation());
        EXPECT_EQ(quat, rbt.get_rotation());
    }

    {
        gn::math::Vector3 vec(1, 2, 3);
        gn::math::Rbt rbt(vec);
        EXPECT_EQ(vec, rbt.get_translation());
        EXPECT_EQ(gn::math::Quaternion(), rbt.get_rotation());
    }

    {
        gn::math::Quaternion quat(1, 1, 1, 0);
        gn::math::Rbt rbt(quat);
        EXPECT_EQ(gn::math::Vector3(), rbt.get_translation());
        EXPECT_EQ(quat, rbt.get_rotation());
    }
}

TEST(Math_RigidBodyTransformation, arithmetic_operator)
{
    // *
    {
        gn::math::Rbt rbt = gn::math::Rbt(gn::math::Vector3(1, 2, 3), gn::math::Quaternion(1, 1, 0, 0));

        //rbt * rbt
        gn::math::Matrix4 expected_result = Math_RigidBodyTransformation::get_matrix_result_test_multiply();
        expected_result = expected_result * expected_result;
        gn::math::Matrix4 result;
        EXPECT_NO_THROW(result = (rbt * rbt).to_matrix());
        EXPECT_EQ(expected_result, result);

        //rbt * vec4
        gn::math::Vector4 expected_vec_result = Math_RigidBodyTransformation::get_matrix_result_test_multiply() * gn::math::Vector4(1, 2, 3, 4);
        gn::math::Vector4 result_vec;

        EXPECT_NO_THROW(result_vec = rbt * gn::math::Vector4(1, 2, 3, 4));
        EXPECT_EQ(expected_vec_result, result_vec);
    }
}

TEST(Math_RigidBodyTransformation, get_inverse)
{
    gn::math::Rbt rbt = gn::math::Rbt(gn::math::Vector3(1, 2, 3), gn::math::Quaternion(1, 1, 0, 0));

    gn::math::Matrix4 expected_result;
    expected_result(1, 1) = 0; 
    expected_result(2, 1) = 1;
    expected_result(1, 2) = -1;
    expected_result(2, 2) = 0;
    expected_result(0, 3) = 1;
    expected_result(1, 3) = 2;
    expected_result(2, 3) = 3;
    
    gn::math::Rbt result;
    EXPECT_NO_THROW(result = rbt.get_inverse());
    EXPECT_EQ(expected_result.get_inverse_matrix(), result.to_matrix());
}

TEST(Math_RigidBodyTransformation, get_translation_linear_factor)
{
    gn::math::Vector3 vec(1, 2, 3);
    gn::math::Quaternion quat(1, 1, 1, 0);
    gn::math::Rbt rbt(vec, quat);

    gn::math::Rbt result;
    //translation factor
    EXPECT_NO_THROW(result = rbt.get_translation_factor());
    EXPECT_EQ(vec, result.get_translation());
    EXPECT_EQ(gn::math::Quaternion(), result.get_rotation());

    //linear factor
    EXPECT_NO_THROW(result = rbt.get_linear_factor());
    EXPECT_EQ(gn::math::Vector3(), result.get_translation());
    EXPECT_EQ(quat, result.get_rotation());
}

TEST(Math_RigidBodyTransformation, to_matrix)
{
    gn::math::Rbt rbt;
    gn::math::Matrix4 result;
    EXPECT_NO_THROW(result = rbt.to_matrix());
    EXPECT_EQ(gn::math::Matrix4(), result);

    rbt = gn::math::Rbt(gn::math::Vector3(1, 2, 3), gn::math::Quaternion(1, 1, 0, 0));
    gn::math::Matrix4 expected_result;
    expected_result(1, 1) = 0; 
    expected_result(2, 1) = 1;
    expected_result(1, 2) = -1;
    expected_result(2, 2) = 0;
    expected_result(0, 3) = 1;
    expected_result(1, 3) = 2;
    expected_result(2, 3) = 3;

    EXPECT_NO_THROW(result = rbt.to_matrix());
    EXPECT_EQ(expected_result, result);
}
