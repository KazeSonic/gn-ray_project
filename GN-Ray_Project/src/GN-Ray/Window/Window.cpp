#include "GN-Ray/Window/Window.hpp"

namespace gn
{
    //-----------------------------------------------------------------------------------------
    Window::Window(const uint32_t width, const uint32_t height, const std::string &name) 
        : _width(width),
          _height(height),
          _window(sf::VideoMode(width, height), name),
          _array(sf::Points, width * height)
    {
        this->_buffer.resize(width * height);
        for(uint32_t y = 0; y < height; ++y)
        {
            for(uint32_t x = 0; x < width; ++x)
            {
                this->_array[x + y * width].position.x = (float)x;
                this->_array[x + y * width].position.y = (float)y;
            }
        }
    }

    //-----------------------------------------------------------------------------------------
    void Window::display()
    {
        this->_window.clear();
        for(uint32_t y = 0; y < this->_height; ++y)
        {
            for(uint32_t x = 0; x < this->_width; ++x)
            {
                this->_array[x + y * this->_width].color.r = this->_buffer[x + y * this->_width][0] * 255;
                this->_array[x + y * this->_width].color.g = this->_buffer[x + y * this->_width][1] * 255;
                this->_array[x + y * this->_width].color.b = this->_buffer[x + y * this->_width][2] * 255;
            }
        }

        this->_window.draw(this->_array);
        this->_window.display();
    }


} //namespace gn
