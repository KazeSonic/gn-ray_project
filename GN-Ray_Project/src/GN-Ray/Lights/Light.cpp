#include "GN-Ray/Lights/Light.hpp"

namespace gn
{
namespace lights
{
    //-----------------------------------------------------------------------------------------
    Light::Light() : _color(255, 255, 255)
    {}

    //-----------------------------------------------------------------------------------------
    Light::~Light()
    {}

    //-----------------------------------------------------------------------------------------
    bool Light::light_me(const ::gn::math::Rbt &rbt, const ::gn::ray::Ray &ray)
    {
        return true;
    }
} //namespace lights
} //namespace gn
