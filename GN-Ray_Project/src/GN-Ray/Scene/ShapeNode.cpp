#include "GN-Ray/Scene/ShapeNode.hpp"
#include "GN-Ray/Scene/SceneVisitor.hpp"

namespace gn
{
namespace scene
{
    //-----------------------------------------------------------------------------------------
    ShapeNode::ShapeNode() : _shape(nullptr)
    {}

    //-----------------------------------------------------------------------------------------
    ShapeNode::~ShapeNode()
    {}

    //-----------------------------------------------------------------------------------------
    bool ShapeNode::accept(SceneVisitor &visitor)
    {
        return visitor.visit(*this);
    }

    //-----------------------------------------------------------------------------------------
    float ShapeNode::hit(const ::gn::math::Rbt &rbt, const ::gn::ray::Ray &ray)
    {
        if(!this->_shape) return -1.f;
        return this->_shape->hit(rbt, ray);
    }

    //-----------------------------------------------------------------------------------------
    bool ShapeNode::get_normal_at_intersection(const ::gn::math::Rbt &rbt, const ::gn::math::Vector3 &intersection, ::gn::math::Vector3 &normal)
    {
        if(!this->_shape) return false;
        else return this->_shape->get_normal_at_intersection(rbt, intersection, normal);
    }
} //namespace scene
} //namespace gn