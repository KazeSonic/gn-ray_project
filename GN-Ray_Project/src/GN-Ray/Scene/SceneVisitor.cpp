#include "GN-Ray/Scene/SceneVisitor.hpp"
#include "GN-Ray/Scene/Node.hpp"
#include "GN-Ray/Math/RigidBodyTransformation.hpp"

namespace gn
{
namespace scene
{
    //-----------------------------------------------------------------------------------------
    SceneVisitor::SceneVisitor()
    {}

    //-----------------------------------------------------------------------------------------
    SceneVisitor::~SceneVisitor()
    {}

    //-----------------------------------------------------------------------------------------
    bool SceneVisitor::visit(Node &node)
    {
        if(this->_transformation.empty())
        {
            this->_transformation.push(node.get_rbt());
        }
        else
        {
            this->_transformation.push(this->_transformation.top() * node.get_rbt());
        }

        return true;
    }

    //-----------------------------------------------------------------------------------------
    bool SceneVisitor::visit(ShapeNode &node)
    {
        this->_linearScene.push_back(std::make_pair(this->_transformation.top(), &node));
        return true;
    }

    //-----------------------------------------------------------------------------------------
    bool SceneVisitor::visit(LightNode &node)
    {
        this->_lights.push_back(std::make_pair(this->_transformation.top(), &node));
        return true;
    }

    //-----------------------------------------------------------------------------------------
    bool SceneVisitor::post_visit(Node &node)
    {
        //we did something wrong because we popped to much
        if(this->_transformation.empty()) return false;
        else
        {
            this->_transformation.pop();
            return true;
        }
    }

    //-----------------------------------------------------------------------------------------
    void SceneVisitor::clear()
    {
        this->_linearScene.clear();
        this->_lights.clear();
    }

} //namespace scene
} //namespace gn