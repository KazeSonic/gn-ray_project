#include "GN-Ray/Scene/LightNode.hpp"
#include "GN-Ray/Scene/SceneVisitor.hpp"

namespace gn
{
namespace scene
{
    //-----------------------------------------------------------------------------------------
    LightNode::LightNode()
    {}

    //-----------------------------------------------------------------------------------------
    LightNode::~LightNode()
    {}

    //-----------------------------------------------------------------------------------------
    bool LightNode::accept(SceneVisitor &visitor)
    {
        return visitor.visit(*this);
    }

    //-----------------------------------------------------------------------------------------
    bool LightNode::light_me(const ::gn::math::Rbt &rbt, const ::gn::ray::Ray &ray)
    {
        return this->_light->light_me(rbt, ray);
    }
} //namespace scene
} //namespace gn