#include "GN-Ray/Scene/Node.hpp"
#include "GN-Ray/Scene/SceneVisitor.hpp"

namespace gn
{
namespace scene
{
    //-----------------------------------------------------------------------------------------
    Node::Node()
    {}

    //-----------------------------------------------------------------------------------------
    Node::~Node()
    {}

    //-----------------------------------------------------------------------------------------
    bool Node::accept(SceneVisitor &visitor)
    {
        if(!visitor.visit(*this)) return false;
        for(auto it = this->_children.begin(); it != this->_children.end(); ++it)
        {
            if(!(*it)->accept(visitor)) return false;
        }

        return visitor.post_visit(*this);
    }

    //-----------------------------------------------------------------------------------------
    void Node::add_child(AbstractNode *child)
    {
        if(child) this->_children.insert(child);
    }

    //-----------------------------------------------------------------------------------------
    void Node::remove_child(AbstractNode *child)
    {
        if(child)
        {
            auto it = this->_children.find(child);
            if(it != this->_children.end())
            {
                this->_children.erase(it);
            }
        }
    }

    //-----------------------------------------------------------------------------------------
    void Node::clear_children()
    {
        this->_children.clear();
    }

} //namespace scene
} //namespace gn