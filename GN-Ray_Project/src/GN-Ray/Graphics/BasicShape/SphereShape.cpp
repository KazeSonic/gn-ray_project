#include "GN-Ray/Graphics/BasicShape/SphereShape.hpp"

namespace gn
{
namespace graphics
{
    //-----------------------------------------------------------------------------------------
    SphereShape::SphereShape() : Shape(), _radius(1.f)
    {}

    //-----------------------------------------------------------------------------------------
    SphereShape::~SphereShape()
    {}

    //-----------------------------------------------------------------------------------------
    float SphereShape::hit(const ::gn::math::Rbt &rbt, const ::gn::ray::Ray &ray)
    {
        float delta, t0 , t1;

        float a = 1.0; //because ray direction vector is a unit vector

        ::gn::math::Vector3 l = ray.origin - rbt.get_translation();
        float b = l.dot_product(ray.direction) * 2.f;
        float c = l.dot_product(l) - this->_radius * this->_radius;
        delta = b * b - 4 * c * a;
        if(delta < 0.f)
            return -1.f;

        float sqrt_delta = std::sqrt(delta);
        t0 = (-b - sqrt_delta) * 0.5f; // 0.5 because it's 1 / (2 * a) and a = 1
        if(::gn::math::utils::almost_equal(delta, 0.f))
        {
            return t0;
        }

        t1 = ( -b + sqrt_delta)  * 0.5f; // 0.5 because it's 1 / (2 * a) and a = 1

        if(t0 > t1) std::swap(t0, t1);
        if(t0 < 0.f)
        {
            t0 = t1; //check T1
            if(t1 < 0.0) return -1;
        }

        return t0;
    }

    //-----------------------------------------------------------------------------------------
    bool SphereShape::get_normal_at_intersection(const ::gn::math::Rbt &rbt, const ::gn::math::Vector3 &intersection, ::gn::math::Vector3 &normal)
    {
        normal = (intersection - rbt.get_translation()).normalize();
        return true;
    }
} //namespace graphics
} //namespace gn