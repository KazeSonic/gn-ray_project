#include "GN-Ray/Graphics/Shape.hpp"

#include <math.h>

#include "GN-Ray/Math/Vector.hpp"

namespace gn
{
namespace graphics
{
    //-----------------------------------------------------------------------------------------
    Shape::Shape()
    {}

    //-----------------------------------------------------------------------------------------
    Shape::~Shape()
    {}

    //-----------------------------------------------------------------------------------------
    float Shape::hit(const ::gn::math::Rbt &rbt, const ::gn::ray::Ray &ray)
    {
        return -1.f;
    }

    //-----------------------------------------------------------------------------------------
    bool Shape::get_normal_at_intersection(const ::gn::math::Rbt &rbt, const ::gn::math::Vector3 &intersection, ::gn::math::Vector3 &normal)
    {
        return false;
    }

} //namespace graphics
} //namespace gn
