#include "GN-Ray/Camera/Camera.hpp"

#include <cmath>

namespace gn
{
    //-----------------------------------------------------------------------------------------
    Camera::Camera() : _fov(PI / 2.0f)
    {}

    //-----------------------------------------------------------------------------------------
    Camera::~Camera()
    {}

    //-----------------------------------------------------------------------------------------
    ::gn::math::Vector3 Camera::get_ray_origin(float screenX /* = 0.f */, float screenY /* = 0.f */) const
    {
        return this->_rbt.get_translation();
    }

    //-----------------------------------------------------------------------------------------
    ::gn::math::Vector3 Camera::get_ray_direction(float pixelX, float pixelY) const
    {
        return ::gn::math::Vector3(this->_rbt * ::gn::math::Vector4(pixelX, pixelY, -1, 1)).normalize();
    }

    //-----------------------------------------------------------------------------------------
    float Camera::get_camera_scale() const
    {
        return std::tan(this->_fov / 2.0f);
    }



} //namespace gn