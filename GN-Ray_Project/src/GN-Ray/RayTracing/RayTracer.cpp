#include "GN-Ray/RayTracing/RayTracer.hpp"

#include <limits>
#include <algorithm>

#include "GN-Ray/Camera/Camera.hpp"
#include "GN-Ray/Scene/SceneGraph.hpp"
#include "GN-Ray/Window/Window.hpp"
#include "GN-Ray/RayTracing/Ray.hpp"

namespace gn
{
namespace ray
{
    //-----------------------------------------------------------------------------------------
    void RayTracer::ray_trace(::gn::Window &window, ::gn::Camera &camera, ::gn::scene::SceneGraph &scene)
    {
        this->clear();
        //transform the scene to an array
        scene.get_root().accept(*this);

        //get the color buffer
        std::vector<::gn::graphics::Color> &buffer = window.get_buffer();

        // -- Camera data
        float cameraScale = camera.get_camera_scale();
        float aspectRatio = (float)window.get_width() / (float)window.get_height();
        float one_over_width = 1.f / window.get_width();
        float one_over_height = 1.f / window.get_height();
        // -- -----------

        for(uint32_t y = 0; y < window.get_height(); ++y)
        {
            for(uint32_t x = 0; x < window.get_width(); ++x)
            {
                //build the ray
                float NDCx = (x + 0.5f) * one_over_width;
                float NDCy = (y + 0.5f) * one_over_height;
                float pixelX = (2.f * NDCx - 1.f) * aspectRatio * cameraScale;
                float pixelY = (1.f - 2.f * NDCy) * cameraScale;

                Ray ray;
                ray.origin = camera.get_ray_origin();
                ray.direction = camera.get_ray_direction(pixelX, pixelY);

                buffer[x + y * window.get_width()].reset(); //background for the moment is black
                this->recursive_trace(ray, buffer[x + y * window.get_width()]);
            }
        }
    }

    //-----------------------------------------------------------------------------------------
    bool RayTracer::recursive_trace(::gn::ray::Ray &ray, ::gn::graphics::Color &color, int32_t depth /* = 0 */)
    {
        float currentHit, nearestHit = std::numeric_limits<float>::infinity();
        int nearestObject = -1;

        //first search nearest intersection
        for(uint32_t i = 0; i < this->_linearScene.size(); ++i)
        {
            currentHit = this->_linearScene[i].second->hit(this->_linearScene[i].first, ray);
            if(currentHit > 0.f && currentHit < nearestHit)
            {
                nearestHit = currentHit;
                nearestObject = i;
            }
        }

        //did we hit something ?
        if(nearestObject == -1) return false;

        //::gn::math::Vector3 intersection = ray.origin + ray.direction * nearestHit;
        //::gn::math::Vector3 normal;
        //if(this->_linearScene[nearestObject].second->get_normal_at_intersection(this->_linearScene[nearestObject].first, intersection, normal))
        //{
        //    color[0] = std::max(0.0, normal.dot_product(-ray.direction));
        //}

        ::gn::math::Vector3 normal;

        //handle shadow ray
        Ray shadowRay;
        ::gn::math::Vector3 intersection = ray.origin + ray.direction * nearestHit;
        double kDiffuse = 0;
        for(uint32_t i = 0; i < this->_lights.size(); ++i)
        {
            shadowRay.direction =  (this->_lights[i].first.get_translation() - intersection).normalize();
            shadowRay.origin = intersection + shadowRay.direction * 1e-4;
            //light does not light the intersection point then continue to another light
            if(!this->_lights[i].second->light_me(this->_lights[i].first, shadowRay)) continue;

            //check if there is an object between the light and the 
            if(!this->trace_shadow_ray(shadowRay))
            {
                if(this->_linearScene[nearestObject].second->get_normal_at_intersection(this->_linearScene[nearestObject].first, intersection, normal))
                {
                    kDiffuse = std::max(0.0, normal.dot_product(shadowRay.direction));
                    color[0] = kDiffuse * 1.0;
                }
            }
        }

        return true;
    }

    //-----------------------------------------------------------------------------------------
    bool RayTracer::trace_shadow_ray(::gn::ray::Ray &ray)
    {
        float currentHit, nearestHit = std::numeric_limits<float>::infinity();
        int nearestObject = -1;
        //first search nearintersection
        for(uint32_t i = 0; i < this->_linearScene.size(); ++i)
        {
            currentHit = this->_linearScene[i].second->hit(this->_linearScene[i].first, ray);
            if(currentHit > 0.f && currentHit < nearestHit)
            {
                nearestHit = currentHit;
                nearestObject = i;
            }
        }

        //did we hit something ? if not => we hit the light !
        if(nearestObject != -1) return true;
        else return false;
    }


} //namespace ray
} //namespace gn
