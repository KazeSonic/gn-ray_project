#ifndef __VECTOR_HPP__
#define __VECTOR_HPP__

#include <cassert>
#include <cmath>
#include <cstring>
#include <stdint.h>
#include <limits>
#include <algorithm>

#include "GN-Ray/Math/MathUtils.hpp"

namespace gn
{
namespace math
{
    ////////////////////////////////////////////////////////////
    /// \brief Template class to create vector size of 2, 3, 4.
    ///
    /// \type T is the type of the values in the vector
    /// \int  N is the size of the vector
    /// \int  ulp (Units in the Last Place) see : MathUtils.hpp
    ////////////////////////////////////////////////////////////
    template <typename T, int n, uint32_t ulp = 4>
    class Vector
    {
        public:
            ////////////////////////////////////////////////////////////
            /// \brief Default constructor.
            ////////////////////////////////////////////////////////////
            Vector() { memset(this->_v, 0, sizeof(this->_v)); }

            ////////////////////////////////////////////////////////////
            /// \brief Constructor with a default value.
            ////////////////////////////////////////////////////////////
            Vector(const T &value) { std::fill_n(this->_v, n, value); }

            ////////////////////////////////////////////////////////////
            /// \brief Constructor with values for a Vector2.
            ////////////////////////////////////////////////////////////
            Vector(const T &x, const T &y)
            {
                static_assert(n == 2, "Bad vector size - Wrong type used for this number of parameter");
                this->_v[0] = x;
                this->_v[1] = y;
            }

            ////////////////////////////////////////////////////////////
            /// \brief Constructor with values for a Vector3.
            ////////////////////////////////////////////////////////////
            Vector(const T &x, const T &y, const T &z)
            {
                static_assert(n == 3, "Bad vector size - Wrong type used for this number of parameter");
                this->_v[0] = x;
                this->_v[1] = y;
                this->_v[2] = z;
            }

            ////////////////////////////////////////////////////////////
            /// \brief Constructor with values for a Vector4.
            ////////////////////////////////////////////////////////////
            Vector(const T &x, const T &y, const T &z, const T &w)
            {
                static_assert(n == 4, "Bad vector size - Wrong type used for this number of parameter");
                this->_v[0] = x;
                this->_v[1] = y;
                this->_v[2] = z;
                this->_v[3] = w;
            }

            ////////////////////////////////////////////////////////////
            /// \brief Constructor from a different size of vector.
            /// It creates a vector of size n and fill it with the values
            /// of the vector of size m. If n < m the vector is truncated
            /// and if n > m, extendedValue is added to the end.
            ///
            /// \param Vector that need to be copied
            /// \param Extended value(s) if m < n
            ////////////////////////////////////////////////////////////
            template<int m>
            explicit Vector(const Vector<T, m> &vec, const T &extendedValue = T(0))
            {
                for(int32_t i = 0; i < std::min(m, n); ++i) this->_v[i] = vec[i];
                for(int32_t i = std::min(m, n); i < n; ++i) this->_v[i] = extendedValue;
            }

            ////////////////////////////////////////////////////////////
            /// \brief Access operator overload.
            ////////////////////////////////////////////////////////////
            inline T& operator [] (const int i) { return this->_v[i]; }

            inline const T& operator [] (const int i) const { return this->_v[i]; }

            inline T& operator () (const int i) { return this->_v[i]; }

            inline const T& operator () (const int i) const { return this->_v[i]; }

            ////////////////////////////////////////////////////////////
            /// \brief Overload of mathematics operator.
            ////////////////////////////////////////////////////////////
            inline Vector& operator -= (const Vector &other)
            {
                for(uint32_t i = 0; i < n; ++i) this->_v[i] -= other._v[i];
                return *this;
            }

            inline Vector& operator += (const Vector &other)
            {
                for(uint32_t i = 0; i < n; ++i) this->_v[i] += other._v[i];
                return *this;
            }

            inline Vector& operator *= (const T &value)
            {
                for(uint32_t i = 0; i < n; ++i) this->_v[i] *= value;
                return *this;
            }

            inline Vector& operator /= (const T &value)
            {
                const T inverseValue = 1 / value;
                for(uint32_t i = 0; i < n; ++i) this->_v[i] *= inverseValue;
                return *this;
            }

            inline Vector operator + (const Vector &other) const { return Vector(*this) += other; }

            inline Vector operator - () const { return Vector(*this) *= -1; }

            inline Vector operator - (const Vector &other) const { return Vector(*this) -= other; }

            inline Vector operator * (const T &value) const { return Vector(*this) *= value; }

            inline Vector operator / (const T &value) const { return Vector(*this) /= value; }

            ////////////////////////////////////////////////////////////
            /// \brief Overload equals operator (for double and float).
            ////////////////////////////////////////////////////////////
            inline bool operator == (const Vector &other) const
            {
                for(uint32_t i = 0; i < n; ++i)
                {
                    if(!gn::math::utils::almost_equal(this->_v[i], other._v[i], ulp)) return false;
                }
                return true;
            }

            inline bool operator != (const Vector &other) const { return !(*this == other); }

            ////////////////////////////////////////////////////////////
            /// \brief Give the norm of the vector.
            ////////////////////////////////////////////////////////////
            inline T get_norm() const { return std::sqrt(this->dot_product(*this)); }

            ////////////////////////////////////////////////////////////
            /// \brief Normalize the vector.
            ////////////////////////////////////////////////////////////
            inline Vector& normalize()
            {
                assert(this->dot_product(*this) > std::numeric_limits<T>::epsilon());
                return *this /= this->get_norm();
            }

            ////////////////////////////////////////////////////////////
            /// \brief Give the normalize vector of the vector.
            ///
            /// \return Normalize vector
            ///
            ////////////////////////////////////////////////////////////
            inline Vector get_normalize_vector() const
            {
                assert(this->dot_product(*this) > std::numeric_limits<T>::epsilon());
                return*this / this->get_norm();
            }

            ////////////////////////////////////////////////////////////
            /// \brief Dot product between this vector and 
            /// the given vector.
            ///
            /// \params A vector
            ///
            /// \return Dot product between both vector
            ///
            ////////////////////////////////////////////////////////////
            inline T dot_product(const Vector &other) const
            {
                T result = 0;
                for(uint32_t i = 0; i < n; ++i) result += this->_v[i] * other[i];
                return result;
            }

            ////////////////////////////////////////////////////////////
            /// \brief Give the cross product between this vector and 
            /// the given vector.
            ///
            /// \params A vector
            ///
            /// \return The cross product vector
            ///
            ////////////////////////////////////////////////////////////
            Vector<T, 3> cross_product(const Vector<T, 3> &other) const
            //inline Vector<T, 3> cross_product(const Vector<T, 3> &second) const
            {
                static_assert(n == 3, "Bad vector size - Wrong type used for *this");
                return Vector<T, 3>(this->_v[1] * other[2] - this->_v[2] * other[1], //multiplied by +1
                                    this->_v[2] * other[0] - this->_v[0] * other[2], //multiplied by -1
                                    this->_v[0] * other[1] - this->_v[1] * other[0]);//multiplied by +1
            }

        private:
            ////////////////////////////////////////////////////////////
            /// Member data
            ////////////////////////////////////////////////////////////
            T _v[n];
    }; //End class Vector

    //Element of type double precision float
    typedef Vector <double, 2> Vector2;
    typedef Vector <double, 3> Vector3;
    typedef Vector <double, 4> Vector4;

    //Element of type single precision float
    typedef Vector <float, 2> Vector2f;
    typedef Vector <float, 3> Vector3f;
    typedef Vector <float, 4> Vector4f;

    //Element of type unsigned byte
    typedef Vector <uint8_t, 2> Vector2b;
    typedef Vector <uint8_t, 3> Vector3b;
    typedef Vector <uint8_t, 4> Vector4b;
} //namespace math
} //namespace gn

#endif //__VECTOR_HPP__