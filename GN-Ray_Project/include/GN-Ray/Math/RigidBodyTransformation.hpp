#ifndef __RIGIDBODYTRANSFORMATION_HPP__
#define __RIGIDBODYTRANSFORMATION_HPP__

#include <stdint.h>

#include "GN-Ray/Math/Matrix4.hpp"
#include "GN-Ray/Math/Quaternion.hpp"
#include "GN-Ray/Math/Vector.hpp"

namespace gn
{
namespace math
{
    ////////////////////////////////////////////////////////////
    /// \brief The RigidBodyTransformation class used in order to
    /// simplify the representation of a non-scaled transformation.
    /// It contains a Vector3 for the position and a Quaternion
    /// for the rotation.
    ////////////////////////////////////////////////////////////
    class RigidBodyTransformation
    {
        public:

            ////////////////////////////////////////////////////////////
            /// \brief Default constructor.
            /// It creates a RigidBodyTransformation with [0, 0, 0]
            /// translation and no rotation.
            ////////////////////////////////////////////////////////////
            RigidBodyTransformation() {}

            ////////////////////////////////////////////////////////////
            /// \brief Constructor from given translation and rotation.
            ////////////////////////////////////////////////////////////
            RigidBodyTransformation(const Vector3 &translation, const Quaternion &rotation)
            {
                this->_translation = translation;
                this->_rotation = rotation;
            }

            ////////////////////////////////////////////////////////////
            /// \brief Constructor with a translation only (no rotation).
            ////////////////////////////////////////////////////////////
            explicit RigidBodyTransformation(const Vector3 &translation)
            {
                this->_translation = translation;
                this->_rotation = Quaternion();
            }

            ////////////////////////////////////////////////////////////
            /// \brief Constructor with a rotation only (no translation).
            ////////////////////////////////////////////////////////////
            explicit RigidBodyTransformation(const Quaternion &rotation)
            {
                this->_translation = Vector3();
                this->_rotation = rotation;
            }

            ////////////////////////////////////////////////////////////
            /// \brief Getters / Setters
            ////////////////////////////////////////////////////////////
            inline Vector3& get_translation() { return this->_translation; }

            inline const Vector3& get_translation() const { return this->_translation; }

            inline Quaternion& get_rotation() { return this->_rotation; }

            inline const Quaternion& get_rotation() const { return this->_rotation; }

            inline void set_translation(const Vector3 &translation) { this->_translation = translation; }

            inline void set_rotation(const Quaternion &rotation) { this->_rotation = rotation; }

            ////////////////////////////////////////////////////////////
            /// \brief Overload of mathematics operator.
            ////////////////////////////////////////////////////////////
            RigidBodyTransformation operator * (const RigidBodyTransformation &other) const
            {
                return RigidBodyTransformation(this->_translation + Vector3(this->_rotation * Vector4(other._translation, 0)),
                                               this->_rotation * other._rotation);
            }

            Vector4 operator * (const Vector4 &other) const
            {
                return Vector4(this->_translation, 0) * other[3] + this->_rotation *other;
            }

            ////////////////////////////////////////////////////////////
            /// \brief Overload equals operator.
            ////////////////////////////////////////////////////////////
            bool operator == (const RigidBodyTransformation &other) const
            {
                return (this->_translation == other._translation) &&
                       (this->_rotation == other._rotation);
            }

            bool operator != (const RigidBodyTransformation &other) const
            {
                return !(*this == other);
            }

            ////////////////////////////////////////////////////////////
            /// \brief Gives the inverse of the RigidBodyTransformation
            ///
            /// \return Inverse of the Rbt
            ////////////////////////////////////////////////////////////
            RigidBodyTransformation get_inverse() const
            {
                Quaternion rotation = this->_rotation.get_inverse();
                Vector4 translation = rotation * - Vector4(this->_translation, 0);
                return RigidBodyTransformation(Vector3(translation), rotation);
            }

            ////////////////////////////////////////////////////////////
            /// \brief Gives a Rbt with only the translation transformation
            /// of the current Rbt.
            ///
            /// \return Translation Rbt
            ////////////////////////////////////////////////////////////
            inline RigidBodyTransformation get_translation_factor() const
            {
                return RigidBodyTransformation(this->_translation);
            }

            ////////////////////////////////////////////////////////////
            /// \brief Gives a Rbt with only the linear transformation
            /// of the current Rbt.
            ///
            /// \return Linear Rbt
            ////////////////////////////////////////////////////////////
            inline RigidBodyTransformation get_linear_factor() const
            {
                return RigidBodyTransformation(this->_rotation);
            }

            ////////////////////////////////////////////////////////////
            /// \brief Gives the matrix form of the current RigidBodyTransformation.
            ///
            /// \return Rbt to the matrix form
            ////////////////////////////////////////////////////////////
            Matrix4 to_matrix() const
            {
                Matrix4 matrix = this->_rotation.to_matrix();

                for(uint32_t i = 0; i < 3; ++i)
                {
                    matrix(i, 3) = this->_translation[i];
                }

                return matrix;
            }
            
        private:
            ////////////////////////////////////////////////////////////
            /// Member data
            ////////////////////////////////////////////////////////////
            Vector3     _translation;   ///< Translation component
            Quaternion  _rotation;      ///< Quaternion (Rotation) component
    }; //End class RigidBodyTransformation

    typedef RigidBodyTransformation Rbt;
} //namespace math
} //namespace gn

#endif //__RIGIDBODYTRANSFORMATION_HPP__