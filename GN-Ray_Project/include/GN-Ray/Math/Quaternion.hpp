#ifndef __QUATERNION_HPP__
#define __QUATERNION_HPP__

#include <cassert>
#include <cmath>
#include <stdint.h>
#include <limits>

#include "GN-Ray/Math/MathConstant.hpp"
#include "GN-Ray/Math/Vector.hpp"
#include "GN-Ray/Math/Matrix4.hpp"

namespace gn
{
namespace math
{
    ////////////////////////////////////////////////////////////
    /// \brief Class for quaternion
    ////////////////////////////////////////////////////////////
    class Quaternion
    {
        public:

            Quaternion() : _q(1.0, 0.0, 0.0, 0.0) {}

            Quaternion(const Vector4 &vec) : _q(vec) {}

            Quaternion(const double w, const Vector3 &vec) : _q(w, vec[0], vec[1], vec[2]) {}

            Quaternion(const double w, const double x, const double y, const double z) : _q(w, x, y, z) {}

            ////////////////////////////////////////////////////////////
            /// \brief Access operator overload.
            ////////////////////////////////////////////////////////////
            inline double& operator [] (const int i) { return this->_q[i]; }

            inline const double& operator [] (const int i) const { return this->_q[i]; }

            inline double& operator () (const int i) { return this->_q[i]; }

            inline const double& operator () (const int i) const { return this->_q[i]; }

            ////////////////////////////////////////////////////////////
            /// \brief Overload of mathematics operator.
            ////////////////////////////////////////////////////////////
            inline Quaternion& operator -= (const Quaternion &other)
            {
                this->_q -= other._q;
                return *this;
            }

            inline Quaternion& operator += (const Quaternion &other)
            {
                this->_q += other._q;
                return *this;
            }

            inline Quaternion& operator *= (const double &value)
            {
                this->_q *= value;
                return *this;
            }

            inline Quaternion& operator /= (const double &value)
            {
                this->_q /= value;
                return *this;
            }

            inline Quaternion operator - (const Quaternion &other) const { return Quaternion(*this) -= other; }

            inline Quaternion operator - () const { return Quaternion(- this->_q); }

            inline Quaternion operator + (const Quaternion &other) const { return Quaternion(*this) += other; }

            inline Quaternion operator * (const Quaternion &other) const
            {
                return Quaternion(other[0] * this->_q[0] - other[1] * this->_q[1] - other[2] * this->_q[2] - other[3] * this->_q[3],
                                  other[0] * this->_q[1] + other[1] * this->_q[0] - other[2] * this->_q[3] + other[3] * this->_q[2],
                                  other[0] * this->_q[2] + other[1] * this->_q[3] + other[2] * this->_q[0] - other[3] * this->_q[1],
                                  other[0] * this->_q[3] - other[1] * this->_q[2] + other[2] * this->_q[1] + other[3] * this->_q[0]);
                /*const Vector3 u(this->_q[1], this->_q[2], this->_q[3]), v(other._q[1], other._q[2], other._q[3]);
                return Quaternion(this->_q[0] * other._q[0] - u.dot_product(v), (v * this->_q[0] + u * other._q[0]) + u.cross_product(v));*/
            }

            inline Vector4 operator * (const Vector4 &vec) const
            {
                Vector4 result;

                double x = this->_q[2] * vec[2] - this->_q[3] * vec[1];
                double y = this->_q[3] * vec[0] - this->_q[1] * vec[2];
                double z = this->_q[1] * vec[1] - this->_q[2] * vec[0];

                x += x; y += y; z += z; //multiply by 2

                result[0] = vec[0] + this->_q[0] * x + this->_q[2] * z - this->_q[3] * y;
                result[1] = vec[1] + this->_q[0] * y + this->_q[3] * x - this->_q[1] * z;
                result[2] = vec[2] + this->_q[0] * z + this->_q[1] * y - this->_q[2] * x;

                result[3] = vec[3];
                return result;

                //Vector4 result;
                
                /*result[0] = (1.0 - 2.0 * this->_q[2] * this->_q[2] - 2.0 * this->_q[3] * this->_q[3]) * vec[0] +
                            (2.0 * (this->_q[1] * this->_q[2] + this->_q[0] * this->_q[3]))       * vec[1] +
                            (2.0 * (this->_q[1] * this->_q[3] - this->_q[0] * this->_q[2]))       * vec[2];

                result[1] = (2.0 * (this->_q[1] * this->_q[2] - this->_q[0] * this->_q[3]))       * vec[0] +
                            (1.0 - 2.0 * this->_q[1] * this->_q[1] - 2.0 * this->_q[3] * this->_q[3]) * vec[1] +
                            (2.0 * (this->_q[2] * this->_q[3] + this->_q[0] * this->_q[1]))       * vec[2];

                result[2] = (2.0 * (this->_q[1] * this->_q[3] + this->_q[0] * this->_q[2]))       * vec[0] +
                            (2.0 * (this->_q[2] * this->_q[3] - this->_q[0] * this->_q[1]))       * vec[1] +
                            (1.0 - 2.0 * this->_q[1] * this->_q[1] - 2.0 * this->_q[2] * this->_q[2]) * vec[2];*/

                /*result[0] = (1.0 - 2.0 * this->_q[2] * this->_q[2] - 2.0 * this->_q[3] * this->_q[3])   * vec[0] +
                            (2.0 * (this->_q[1] * this->_q[2] - this->_q[0] * this->_q[3]))             * vec[1] +
                            (2.0 * (this->_q[1] * this->_q[3] + this->_q[0] * this->_q[2]))             * vec[2];

                result[1] = (2.0 * (this->_q[1] * this->_q[2] + this->_q[0] * this->_q[3]))             * vec[0] +
                            (1.0 - 2.0 * this->_q[1] * this->_q[1] - 2.0 * this->_q[3] * this->_q[3])   * vec[1] +
                            (2.0 * (this->_q[2] * this->_q[3] - this->_q[0] * this->_q[1]))             * vec[2];

                result[2] = (2.0 * (this->_q[1] * this->_q[3] - this->_q[0] * this->_q[2]))             * vec[0] +
                            (2.0 * (this->_q[2] * this->_q[3] + this->_q[0] * this->_q[1]))             * vec[1] +
                            (1.0 - 2.0 * this->_q[1] * this->_q[1] - 2.0 * this->_q[2] * this->_q[2])   * vec[2];

                result[3] = vec[3];*/

                /*Vector3 q3(this->_q[1], this->_q[2], this->_q[3]);
                Vector3 t = q3.cross_product(Vector3(vec)) * 2.0;

                result = Vector4(Vector3(vec) + t * this->_q[0] + Vector3(this->_q[1], this->_q[2], this->_q[3]).cross_product(Vector3(t)), vec[3]);
                return result;*/
                
                /*const Quaternion result = *this * (Quaternion(0, vec[0], vec[1], vec[2]) * this->get_inverse());
                return Vector4(result[1], result[2], result[3], vec[3]);*/
            }

            inline Quaternion operator * (const double value) const { return Quaternion(*this) *= value; }

            inline Quaternion operator / (const double a) const { return Quaternion(*this) /= a; }


            ////////////////////////////////////////////////////////////
            /// \brief Overload equals operator.
            ////////////////////////////////////////////////////////////
            inline bool operator == (const Quaternion &other) const { return this->_q == other._q; }

            inline bool operator != (const Quaternion &other) const { return !(this->_q == other._q); }

            ////////////////////////////////////////////////////////////
            /// \brief Give the inverse quaternion of the quaternion.
            ////////////////////////////////////////////////////////////
            inline Quaternion get_inverse() const
            {
                const double norm = this->get_norm();
                assert(norm > std::numeric_limits<double>::epsilon());
                return Quaternion(this->_q[0], -this->_q[1], -this->_q[2], -this->_q[3]) *= (1.0 / norm);
            }

            ////////////////////////////////////////////////////////////
            /// \brief Give the norm of the quaternion.
            ////////////////////////////////////////////////////////////
            inline double get_norm() const
            {
                return this->dot_product(*this);
            }

            ////////////////////////////////////////////////////////////
            /// \brief Normalize the quaternion.
            ////////////////////////////////////////////////////////////
            inline Quaternion& normalize()
            {
                return *this /= std::sqrt(this->get_norm());
            }

            ////////////////////////////////////////////////////////////
            /// \brief Give the normalize quaternion of the quaternion.
            ///
            /// \return Normalize quaternion
            ///
            ////////////////////////////////////////////////////////////
            inline Quaternion get_normalize_quaternion() const
            {
                return Quaternion(*this / std::sqrt(this->get_norm()));
            }

            ////////////////////////////////////////////////////////////
            /// \brief Dot product between this quaternion and 
            /// the given quaternion.
            ///
            /// \params A quaternion
            ///
            /// \return Dot product between both quaternion
            ///
            ////////////////////////////////////////////////////////////
            inline double dot_product(const Quaternion &other) const
            {
                double result = 0.0;
                for(uint32_t i = 0; i < 4; ++i) result += this->_q[i] * other._q[i];
                return result;
            }

            inline Matrix4 to_matrix() const
            {
                const double norm = this->get_norm();
                if(norm < std::numeric_limits<double>::epsilon()) return Matrix4(0);

                const double two_over_nom = 2.0 / norm;
                Matrix4 result;

                result(0, 0) -= (this->_q[2] * this->_q[2] + this->_q[3] * this->_q[3]) * two_over_nom;
                result(0, 1) += (this->_q[1] * this->_q[2] - this->_q[0] * this->_q[3]) * two_over_nom;
                result(0, 2) += (this->_q[1] * this->_q[3] + this->_q[2] * this->_q[0]) * two_over_nom;

                result(1, 0) += (this->_q[1] * this->_q[2] + this->_q[0] * this->_q[3]) * two_over_nom;
                result(1, 1) -= (this->_q[1] * this->_q[1] + this->_q[3] * this->_q[3]) * two_over_nom;
                result(1, 2) += (this->_q[2] * this->_q[3] - this->_q[1] * this->_q[0]) * two_over_nom;
                
                result(2, 0) += (this->_q[1] * this->_q[3] - this->_q[2] * this->_q[0]) * two_over_nom;
                result(2, 1) += (this->_q[2] * this->_q[3] + this->_q[1] * this->_q[0]) * two_over_nom;
                result(2, 2) -= (this->_q[1] * this->_q[1] + this->_q[2] * this->_q[2]) * two_over_nom;

                assert(result.is_affine());

                return result;
            }


        private:
            ////////////////////////////////////////////////////////////
            /// Member data
            ////////////////////////////////////////////////////////////
            Vector4 _q; // _q[0] = w, _q[1] = x, _q[2] = y, _q[3] = z
    };

    namespace quaternion
    {
        ////////////////////////////////////////////////////////////
        /// \brief Create a rotation quaternion around the X axis.
        ///
        /// \params Angle in degrees
        ///
        /// \return The rotation quaternion
        ///
        ////////////////////////////////////////////////////////////
        inline ::gn::math::Quaternion make_x_rotation(const double angle)
        {
            ::gn::math::Quaternion rotation;
            const double h = 0.5 * angle * PI / 180.0;
            rotation[0] = std::cos(h);
            rotation[1] = std::sin(h);
            return rotation;
        }

        ////////////////////////////////////////////////////////////
        /// \brief Create a rotation quaternion around the Y axis.
        ///
        /// \params Angle in degrees
        ///
        /// \return The rotation quaternion
        ///
        ////////////////////////////////////////////////////////////
        inline ::gn::math::Quaternion make_y_rotation(const double angle)
        {
            ::gn::math::Quaternion rotation;
            const double h = 0.5 * angle * PI / 180.0;
            rotation[0] = std::cos(h);
            rotation[2] = std::sin(h);
            return rotation;
        }

        ////////////////////////////////////////////////////////////
        /// \brief Create a rotation quaternion around the Z axis.
        ///
        /// \params Angle in degrees
        ///
        /// \return The rotation quaternion
        ///
        ////////////////////////////////////////////////////////////
        inline ::gn::math::Quaternion make_z_rotation(const double angle)
        {
            ::gn::math::Quaternion rotation;
            const double h = 0.5 * angle * PI / 180.0;
            rotation[0] = std::cos(h);
            rotation[3] = std::sin(h);
            return rotation;
        }

    } //namespace quaternion


} //namespace math
} //namespace gn

#endif //__QUATERNION_HPP__