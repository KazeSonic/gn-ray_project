#ifndef __MATRIX4_HPP__
#define __MATRIX4_HPP__

#include <cmath>
#include <cstring>
#include <stdint.h>
#include <limits>
#include <algorithm>

#include "GN-Ray/Math/MathUtils.hpp"
#include "GN-Ray/Math/Vector.hpp"


namespace gn
{
namespace math
{
    ////////////////////////////////////////////////////////////
    /// \brief 4-by-4 matrix class. It is a row-major matrix.
    ///        The representation will be : 
    ///        | L T |
    ///        | 0 1 |
    /// with L has the linear transformation and T the translation
    /// transformation.
    ////////////////////////////////////////////////////////////
    class Matrix4
    {
        public:

            ////////////////////////////////////////////////////////////
            /// \brief Default constructor. It gives the identity matrix.
            ////////////////////////////////////////////////////////////
            Matrix4()
            {
                memset(this->_d, 0, sizeof(this->_d));
                for(uint32_t i = 0; i < 4; ++i) this->_d[i * 4 + i] = 1.0;
            }

            Matrix4(const double value)
            {
                for(uint32_t i = 0; i < 16; ++i) this->_d[i] = value;
            }

            ////////////////////////////////////////////////////////////
            /// \brief Access operator overload.
            ////////////////////////////////////////////////////////////
            inline double& operator [] (const int i) { return this->_d[i]; }

            inline const double& operator [] (const int i) const { return this->_d[i]; }

            inline double & operator () (const int row, const int col) { return this->_d[row * 4 + col]; }

            inline const double & operator () (const int row, const int col) const { return this->_d[row * 4 + col]; }

            ////////////////////////////////////////////////////////////
            /// \brief Overload of mathematics operator.
            ////////////////////////////////////////////////////////////
            inline Matrix4& operator -= (const Matrix4 &other)
            {
                for(uint32_t i = 0; i < 16; ++i) this->_d[i] -= other._d[i];
                return *this;
            }

            inline Matrix4& operator += (const Matrix4 &other)
            {
                for(uint32_t i = 0; i < 16; ++i) this->_d[i] += other._d[i];
                return *this;
            }

            inline Matrix4& operator *= (const Matrix4 &other)
            {
                return *this = *this * other;
            }

            inline Matrix4& operator *= (const double value)
            {
                for(uint32_t i = 0; i < 16; ++i) this->_d[i] *= value;
                return *this;
            }

            inline Matrix4 operator - (const Matrix4 &other) const
            {
                return Matrix4(*this) -= other;
            }

            inline Matrix4 operator - () const
            {
                return *this * -1;
            }

            inline Matrix4 operator + (const Matrix4 &other) const
            {
                return Matrix4(*this) += other;
            }

            inline Matrix4 operator * (const Matrix4 &other) const
            {
                Matrix4 result(0);
                for(uint32_t row = 0; row < 4; ++ row)
                    for(uint32_t col = 0; col < 4; ++col)
                        for(uint32_t k = 0; k < 4; ++k)
                            result(row, col) += (*this)(row, k) * other(k, col);
                return result;
            }

            inline Vector4 operator * (const Vector4 &vec) const
            {
                Vector4 result;
                for(uint32_t i = 0; i < 4; ++i)
                    for(uint32_t j = 0; j < 4; ++j)
                        result[i] += (*this)(i, j) * vec[j];
                return result;
            }

            inline Matrix4 operator * (const double value) const { return Matrix4(*this) *= value; }

            ////////////////////////////////////////////////////////////
            /// \brief Overload equals operator.
            ////////////////////////////////////////////////////////////
            inline bool operator == (const Matrix4 &other) const
            {
                for(uint32_t i = 0; i < 16; ++i)
                {
                    if(!gn::math::utils::almost_equal(this->_d[i], other._d[i], 4)) return false;
                }
                return true;
            }

            inline bool operator != (const Matrix4 &other) const { return !(*this == other); }

            ////////////////////////////////////////////////////////////
            /// \brief Gives if the matrix is an affine transformation.
            ///        It supposes the matrix is | L T |
            ///                                  | 0 1 |
            /// with last row is [0 0 0 1].
            ///
            /// \return true if yes, else false
            ////////////////////////////////////////////////////////////
            inline bool is_affine() const
            {
                return std::abs(this->_d[15] - 1) + std::abs(this->_d[14])
                     + std::abs(this->_d[13]) + std::abs(this->_d[12])
                                < std::numeric_limits<double>::epsilon();
            }

            ////////////////////////////////////////////////////////////
            /// \brief Gives the determinant of the matrix. It assumes
            /// the last row of the matrix is [0 0 0 1].
            ///
            /// \return Determinant
            ////////////////////////////////////////////////////////////
            inline double get_determinant() const
            {
                assert(this->is_affine());
                return (*this)(0, 0) * ((*this)(1, 1) * (*this)(2, 2) - (*this)(1, 2) * (*this)(2, 1))
                     - (*this)(0, 1) * ((*this)(1, 0) * (*this)(2, 2) - (*this)(1, 2) * (*this)(2, 0))
                     + (*this)(0, 2) * ((*this)(1, 0) * (*this)(2, 1) - (*this)(1, 1) * (*this)(2, 0));
            }

            ////////////////////////////////////////////////////////////
            /// \brief Gives the inverse of the matrix. It assumes
            /// the last row of the matrix is [0 0 0 1] then :
            ///
            ///    M = | L T | => M^-1 = | L^-1 -L^-1*T |
            ///        | 0 1 |           |  0      1    |
            ///
            /// \return Inverse of the matrix
            ////////////////////////////////////////////////////////////
            Matrix4 get_inverse_matrix() const
            {
                assert(this->is_affine());

                double det = this->get_determinant();
                //non-singular check
                assert(std::abs(det) > std::numeric_limits<double>::epsilon());

                const Matrix4 &matrix = (*this); //easier to read
                Matrix4 inverse;
                double invDet = 1.0 / det; // / cost more than *

                //do L^-1
                inverse(0, 0) =   (matrix(1, 1) * matrix(2, 2) - matrix(1, 2) * matrix(2, 1)) * invDet;
                inverse(0, 1) = - (matrix(0, 1) * matrix(2, 2) - matrix(0, 2) * matrix(2, 1)) * invDet;
                inverse(0, 2) =   (matrix(0, 1) * matrix(1, 2) - matrix(0, 2) * matrix(1, 1)) * invDet;

                inverse(1, 0) = - (matrix(1, 0) * matrix(2, 2) - matrix(1, 2) * matrix(2, 0)) * invDet;
                inverse(1, 1) =   (matrix(0, 0) * matrix(2, 2) - matrix(0, 2) * matrix(2, 0)) * invDet;
                inverse(1, 2) = - (matrix(0, 0) * matrix(1, 2) - matrix(0, 2) * matrix(1, 0)) * invDet;

                inverse(2, 0) =   (matrix(1, 0) * matrix(2, 1) - matrix(1, 1) * matrix(2, 0)) * invDet;
                inverse(2, 1) = - (matrix(0, 0) * matrix(2, 1) - matrix(0, 1) * matrix(2, 0)) * invDet;
                inverse(2, 2) =   (matrix(0, 0) * matrix(1, 1) - matrix(0, 1) * matrix(1, 0)) * invDet;

                //then get -L^-1 * T for the traslation part
                for(uint32_t i = 0; i < 3; ++i)
                {
                    for(uint32_t j = 0; j < 3; ++j)
                    {
                        inverse(i, 3) -= matrix(j, 3) * inverse(i, j);
                    }
                }
                
                return inverse;
            }

            ////////////////////////////////////////////////////////////
            /// \brief Gives the inverse of the matrix using Gauss methode.
            /// /!\ error in precision need to check that
            /// \return Inverse of the matrix
            ////////////////////////////////////////////////////////////
            /*Matrix4 gauss()
            {
                double det = this->get_determinant();
                //non-singular check
                assert(std::abs(det) > std::numeric_limits<double>::epsilon());

                Matrix4 inverse;
                Matrix4 matrix(*this);
                for(uint32_t i = 0; i < 4; ++i)
                {
                    //check if the coef is not null
                    if(utils::almost_equal(matrix(i, i), 0.0, 2))
                    {
                        //swap line with other row ?
                        bool done = false;
                        for(uint32_t row = 0; row < 4; ++row)
                        {
                            if(row != i && !utils::almost_equal(matrix(row, i), 0.0, 2))
                            {
                                //swap row
                                for(uint32_t index = 0; index < 4; ++index)
                                {
                                    std::swap(matrix(i, index), matrix(row, index));
                                    std::swap(inverse(i, index), inverse(row, index));
                                }

                                done = true;
                                break;
                            }
                        }

                        //swap line with other column ?
                        if(!done)
                        {
                            for(uint32_t col = 0; col < 4; ++col)
                            {
                                if(col != i && !utils::almost_equal(matrix(i, col), 0.0, 2))
                                {
                                    //swap row
                                    for(uint32_t index = 0; index < 4; ++index)
                                    {
                                        std::swap(matrix(index, i), matrix(index, col));
                                        std::swap(inverse(index, i), inverse(index, col));
                                    }

                                    done = true;
                                    break;
                                }
                            }
                        }
                        assert(done);
                    }//end if matrix(i, i) == 0.0

                    //devided the by get the 1 in the diagonal
                    double divider = 1.0 / matrix(i, i);
                    for(uint32_t j = 0; j < 4; ++j)
                    {
                        matrix(i, j) *= divider; 
                        inverse(i, j) *= divider; 
                    }

                    //then get 0 in the column i
                    double factor;
                    for(uint32_t row = 0; row < 4; ++row)
                    {
                        if(i != row)
                        {
                            factor = matrix(i, i) * matrix(row, i);
                            for(uint32_t col = 0; col < 4; ++col)
                            {
                                matrix(row, col) -= factor * matrix(i, col);
                                inverse(row, col) -= factor * inverse(i, col);
                            }

                        }
                    }
                }

                return inverse;
            }*/


            ////////////////////////////////////////////////////////////
            /// \brief Gives the transpose of the matrix.
            ///
            /// \return Transposed matrix
            ////////////////////////////////////////////////////////////
            inline Matrix4 get_transpose() const
            {
                Matrix4 transpose;
                for(uint32_t i = 0; i < 4; ++i)
                {
                    for(uint32_t j = 0; j < 4; ++j)
                    {
                        transpose(i, j) = (*this)(j, i);
                    }
                }
                return transpose;
            }

            ////////////////////////////////////////////////////////////
            /// \brief Gives a matrix with only the translation transformation (T)
            /// of the current matrix.
            ///
            /// \return Translation matrix
            ////////////////////////////////////////////////////////////
            inline Matrix4 get_translation_factor() const
            {
                Matrix4 matrix;
                matrix[3] = this->_d[3];
                matrix[7] = this->_d[7];
                matrix[11] = this->_d[11];
                return matrix;
            }

            ////////////////////////////////////////////////////////////
            /// \brief Gives a matrix with only the linear transformation (L)
            /// of the current matrix.
            ///
            /// \return Linear matrix
            ////////////////////////////////////////////////////////////
            inline Matrix4 get_linear_factor() const
            {
                Matrix4 matrix;
                matrix[0] = this->_d[0]; matrix[1] = this->_d[1]; matrix[2] = this->_d[2];
				matrix[4] = this->_d[4]; matrix[5] = this->_d[5]; matrix[6] = this->_d[6];
				matrix[8] = this->_d[8]; matrix[9] = this->_d[9]; matrix[10] = this->_d[10];
                return matrix;
            }

        private:
            ////////////////////////////////////////////////////////////
            /// Member data
            ////////////////////////////////////////////////////////////
            double _d[16];
    };

} //namespace math
} //namespace gn

#endif //__MATRIX4_HPP__