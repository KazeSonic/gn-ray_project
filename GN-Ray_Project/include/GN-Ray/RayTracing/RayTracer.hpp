#ifndef __RAYTRACER_HPP__
#define __RAYTRACER_HPP__

#include <stdint.h>

#include "GN-Ray/Scene/SceneVisitor.hpp"

namespace gn
{
    class Camera;
    class Window;
    
    namespace scene
    {
        class SceneGraph;

    } //namespace scene
    namespace graphics
    {
        class Color;
    } //namespace graphics
    namespace ray
    {
        struct Ray;
    } //namespace ray
} //namespace gn

namespace gn
{
namespace ray
{
    ////////////////////////////////////////////////////////////
    /// \brief Utility class that makes any derived
    /// class non-copyable.
    ////////////////////////////////////////////////////////////
    class RayTracer : public ::gn::scene::SceneVisitor
    {
        public:
            ////////////////////////////////////////////////////////////
			/// \brief Default constructor
            ////////////////////////////////////////////////////////////
            RayTracer () {}

            virtual ~RayTracer() {}

            void ray_trace(::gn::Window &window, ::gn::Camera &camera, ::gn::scene::SceneGraph &scene);

        protected:

            bool recursive_trace(::gn::ray::Ray &ray, ::gn::graphics::Color &color, int32_t depth = 0);

            bool trace_shadow_ray(::gn::ray::Ray &ray);
    };
} //namespace ray
} //namespace gn

#endif //__RAYTRACER_HPP__