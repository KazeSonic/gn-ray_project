#ifndef __RAY_HPP__
#define __RAY_HPP__

#include <GN-Ray/Math.hpp>

namespace gn
{
namespace ray
{    
    ////////////////////////////////////////////////////////////
    /// \brief Class to represent a ray.
    /// Ray = Origin + t * direction
    ////////////////////////////////////////////////////////////
    struct Ray
    {
        ////////////////////////////////////////////////////////////
        /// Member data
        ////////////////////////////////////////////////////////////
        ::gn::math::Vector3 origin;    ///< The origin of the ray;
        ::gn::math::Vector3 direction; ///< In which direction goes the ray;
    };
} //namespace ray
} //namespace gn

#endif //__RAY_HPP__