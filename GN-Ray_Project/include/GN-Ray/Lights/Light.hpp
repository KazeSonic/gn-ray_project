#ifndef __LIGHT_HPP__
#define __LIGHT_HPP__

#include "GN-Ray/Graphics/Color.hpp"
#include "GN-Ray/Math/RigidBodyTransformation.hpp"
#include "GN-Ray/RayTracing/Ray.hpp"

namespace gn
{
namespace lights
{
    ////////////////////////////////////////////////////////////
    /// \brief Basic light class.
    ////////////////////////////////////////////////////////////
    class Light
    {
        public:
            ////////////////////////////////////////////////////////////
			/// \brief Default constructor. Default color is white.
            ////////////////////////////////////////////////////////////
            Light();

            ////////////////////////////////////////////////////////////
			/// \brief Default destructor.
            ////////////////////////////////////////////////////////////
            virtual ~Light();

            ////////////////////////////////////////////////////////////
            /// \brief Getters / Setters
            ////////////////////////////////////////////////////////////
            inline ::gn::graphics::Color& get_color() { return this->_color; }

            inline const ::gn::graphics::Color& get_color() const { return this->_color; }

            inline void set_color(const ::gn::graphics::Color &color) { this->_color = color; }

            ////////////////////////////////////////////////////////////
            /// \brief Methode to know if the light is lightning in the direction
            /// of the ray origin. (eg: a spot light does not light everywhere)
            /// 
            /// \return True if it lights in the direction of the ray origin
            /// Return false otherwise
            ////////////////////////////////////////////////////////////
            virtual bool light_me(const ::gn::math::Rbt &rbt, const ::gn::ray::Ray &ray);

        protected:
            ////////////////////////////////////////////////////////////
            /// Member data
            ////////////////////////////////////////////////////////////
            ::gn::graphics::Color _color; ///< color of the light
    };
} //namespace lights
} //namespace gn

#endif //__LIGHT_HPP__