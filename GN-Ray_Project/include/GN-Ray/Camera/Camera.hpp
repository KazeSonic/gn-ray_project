#ifndef __CAMERA_HPP__
#define __CAMERA_HPP__

#include <stdint.h>

#include <GN-Ray/Math.hpp>

namespace gn
{
    ////////////////////////////////////////////////////////////
    /// \brief Camera class for a basic camera.
    ////////////////////////////////////////////////////////////
    class Camera
    {
        public:
            ////////////////////////////////////////////////////////////
			/// \brief Default constructor.
            /// No translate, No rotation and FOV of Pi / 2 rad.
            ////////////////////////////////////////////////////////////
            Camera();

            ////////////////////////////////////////////////////////////
			/// \brief Default destructor. Do nothing in particular.
            ////////////////////////////////////////////////////////////
            virtual ~Camera();

            ////////////////////////////////////////////////////////////
            /// \brief Getters / Setters
            ////////////////////////////////////////////////////////////
            inline ::gn::math::Rbt& get_rbt() { return this->_rbt; }

            inline const ::gn::math::Rbt& get_rbt() const { return this->_rbt; }

            inline void set_rbt(const ::gn::math::Rbt &rbt) { this->_rbt = rbt; }

            inline const float get_fov() const { return this->_fov; }

            inline void set_fov(const float fov) { this->_fov = fov; }

            ////////////////////////////////////////////////////////////
            /// \brief Give the origin of the ray.
            /// Screen coordinate [X, Y] are added in parameter, it could be
            /// usefull for camera like for example stereoscopic camera.
            ///
            /// \param X from screen coordinate system
            /// \param Y from screen coordinate system
            ///
            /// \return Origin vector
            ////////////////////////////////////////////////////////////
            virtual ::gn::math::Vector3 get_ray_origin(float screenX = 0.f, float screenY = 0.f) const;

            ////////////////////////////////////////////////////////////
            /// \brief Give the direction of the ray based on the pixel
            /// coordinate in 3D coordiante.
            ///
            /// \param X of the pixel
            /// \param Y of the pixel
            ///
            /// \return Direction of the vector
            ////////////////////////////////////////////////////////////
            virtual ::gn::math::Vector3 get_ray_direction(float pixelX, float pixelY) const;

            ////////////////////////////////////////////////////////////
            /// \brief Give the scale of the camera based on the field
            /// of view.
            ///
            /// \return The camera's scale
            ////////////////////////////////////////////////////////////
            virtual float get_camera_scale() const;

        private:
            ////////////////////////////////////////////////////////////
            /// Member data
            ////////////////////////////////////////////////////////////
            ::gn::math::Rbt _rbt; ///< Position and rotation of the camera
            float           _fov; ///< Field of view of the camera in radian
    };
} //namespace gn

#endif //__CAMERA_HPP__