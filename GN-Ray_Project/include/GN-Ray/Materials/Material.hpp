#ifndef __MATERIAL_HPP__
#define __MATERIAL_HPP__

#include "GN-Ray/Graphics/Color.hpp"

namespace gn
{
namespace materials
{
    class Material
    {
        public:
            
            Material();

            virtual ~Material();

            virtual void get_color_at_point(const double textureX, const double textureY, ::gn::graphics::Color &color);

        private:
            ////////////////////////////////////////////////////////////
            /// Member data
            ////////////////////////////////////////////////////////////
            ::gn::graphics::Color _color; ///< Color 
    };
} //namespace materials
} //namespace gn

#endif //__MATERIAL_HPP__