#ifndef __SCENE_HPP__
#define __SCENE_HPP__

#include <GN-Ray/Scene/AbstractNode.hpp>
#include <GN-Ray/Scene/Node.hpp>
#include <GN-Ray/Scene/SceneGraph.hpp>
#include <GN-Ray/Scene/SceneVisitor.hpp>
#include <GN-Ray/Scene/ShapeNode.hpp>

#endif //__SCENE_HPP__