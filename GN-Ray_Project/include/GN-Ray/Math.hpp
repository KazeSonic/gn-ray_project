#ifndef __MATH_HPP__
#define __MATH_HPP__

#include <GN-Ray/Math/MathConstant.hpp>
#include <GN-Ray/Math/MathUtils.hpp>
#include <GN-Ray/Math/Matrix4.hpp>
#include <GN-Ray/Math/Quaternion.hpp>
#include <GN-Ray/Math/RigidBodyTransformation.hpp>
#include <GN-Ray/Math/Vector.hpp>

#endif //__MATH_HPP__