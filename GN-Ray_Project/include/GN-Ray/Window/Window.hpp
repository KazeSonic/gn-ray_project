#ifndef __WINDOW_HPP__
#define __WINDOW_HPP__

#include <stdint.h>
#include <vector>

#include <SFML/Graphics.hpp>

#include <GN-Ray/Graphics.hpp>

namespace gn
{
    ////////////////////////////////////////////////////////////
    /// \brief Utility class for the colors.
    ////////////////////////////////////////////////////////////
    class Window
    {
        public:
            ////////////////////////////////////////////////////////////
			/// \brief Default constructor. The default color is black.
            ////////////////////////////////////////////////////////////
            Window(const uint32_t width, const uint32_t height, const std::string &name);

            inline sf::RenderWindow& get_SFMLWindow() { return this->_window; }

            inline std::vector<::gn::graphics::Color>& get_buffer() { return this->_buffer; }

            inline uint32_t get_width() { return this->_width; }

            inline uint32_t get_height() { return this->_height; }


            bool is_open() const { return this->_window.isOpen(); }

            void display();

        private:
            ////////////////////////////////////////////////////////////
            /// Member data
            ////////////////////////////////////////////////////////////
            uint32_t _width;
            uint32_t _height;
            sf::RenderWindow _window;
            std::vector<::gn::graphics::Color> _buffer;
            sf::VertexArray _array;
    };
} //namespace gn

#endif //__WINDOW_HPP__