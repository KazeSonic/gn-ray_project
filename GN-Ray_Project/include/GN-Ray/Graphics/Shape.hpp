#ifndef __SHAPE_HPP__
#define __SHAPE_HPP__

//#include <vector>

#include "GN-Ray/Math/RigidBodyTransformation.hpp"
#include "GN-Ray/RayTracing/Ray.hpp"

namespace gn
{
namespace graphics
{
    ////////////////////////////////////////////////////////////
    /// \brief Class that handle a 3D object.
    ////////////////////////////////////////////////////////////
    class Shape
    {
        public:
            ////////////////////////////////////////////////////////////
			/// \brief Default constructor.
            ////////////////////////////////////////////////////////////
            Shape();

            ////////////////////////////////////////////////////////////
			/// \brief Default destructor.
            ////////////////////////////////////////////////////////////
            virtual ~Shape();

            ////////////////////////////////////////////////////////////
			/// \brief Methode to check if a ray hit this object.
            /// It is a virtual methode. You can override it in order to
            /// process your own hit function.
            /// 
            /// NOT DONE YET : The default behaviour will check for all triangle
            /// made by the vertice if the ray it one of them.
            ///
            /// \param Position and Rotation of the shape in the space
            /// \param Ray we will check if it hits the shape
            /// 
            /// \return distance between the ray origin and the intersection.
            /// if there is no intersection it will return -1.f
            ///
            ////////////////////////////////////////////////////////////
            virtual float hit(const ::gn::math::Rbt &rbt, const ::gn::ray::Ray &ray);

            virtual bool get_normal_at_intersection(const ::gn::math::Rbt &rbt, const ::gn::math::Vector3 &intersection, ::gn::math::Vector3 &normal);

        protected:
            ////////////////////////////////////////////////////////////
            /// Member data
            ////////////////////////////////////////////////////////////
            //std::vector<Vertex> _vertexArray; ///< Position of the vertex[x y z]
    };
} //namespace graphics
} //namespace gn

#endif //__SHAPE_HPP__