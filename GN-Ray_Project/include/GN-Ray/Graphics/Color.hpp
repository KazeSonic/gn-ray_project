#ifndef __COLOR_HPP__
#define __COLOR_HPP__

#include <stdint.h>

#include "GN-Ray/Math/Vector.hpp"

namespace gn
{
namespace graphics
{
    ////////////////////////////////////////////////////////////
    /// \brief Utility class for the colors.
    ////////////////////////////////////////////////////////////
    class Color
    {
        public:
            ////////////////////////////////////////////////////////////
			/// \brief Default constructor. The default color is black.
            ////////////////////////////////////////////////////////////
            Color() {}

            ////////////////////////////////////////////////////////////
			/// \brief Constructor with default values set.
            ///
            /// \param Value of red
            /// \param Value of green
            /// \param Value of blue
            ///
            ////////////////////////////////////////////////////////////
            Color(const uint8_t red, const uint8_t green, const uint8_t blue)
            {
                this->_color = ::gn::math::Vector3f(red, green, blue);
            }

            ////////////////////////////////////////////////////////////
            /// \brief Access operator overload.
            ////////////////////////////////////////////////////////////
            inline float& operator [] (const int i) { return this->_color[i]; }

            inline const float& operator [] (const int i) const { return this->_color[i]; }

            inline float& operator () (const int i) { return this->_color[i]; }

            inline const float& operator () (const int i) const { return this->_color[i]; }

            inline ::gn::math::Vector3f& get_color() { return this->_color; }

            inline const ::gn::math::Vector3f& get_color() const { return this->_color; }

            inline void set_color(const ::gn::math::Vector3f &color) { this->_color = color; }

            inline void reset(const float value = 0) { this->_color = ::gn::math::Vector3f(value); }

        private:
            ////////////////////////////////////////////////////////////
            /// Member data
            ////////////////////////////////////////////////////////////
            ::gn::math::Vector3f _color; ///< x = red, y = green, z = blue;
    };
} //namespace graphics
} //namespace gn

#endif //__COLOR_HPP__