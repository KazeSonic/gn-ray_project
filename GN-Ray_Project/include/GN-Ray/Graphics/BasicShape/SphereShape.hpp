#ifndef __SPHERESHAPE_HPP__
#define __SPHERESHAPE_HPP__

#include "GN-Ray/Graphics/Shape.hpp"

namespace gn
{
namespace graphics
{
    ////////////////////////////////////////////////////////////
    /// \brief Example shape class of a sphere shape.
    ////////////////////////////////////////////////////////////
    class SphereShape : public Shape
    {
        public:
            ////////////////////////////////////////////////////////////
			/// \brief Default constructor.
            ////////////////////////////////////////////////////////////
            SphereShape();

            virtual ~SphereShape();

            ////////////////////////////////////////////////////////////
            /// \brief Getters / Setters
            ////////////////////////////////////////////////////////////
            inline float get_radius() const { return this->_radius; }

            inline void set_radius(const float radius) { this->_radius = radius; }
            
            virtual float hit(const ::gn::math::Rbt &rbt, const ::gn::ray::Ray &ray) override;

            virtual bool get_normal_at_intersection(const ::gn::math::Rbt &rbt, const ::gn::math::Vector3 &intersection, ::gn::math::Vector3 &normal) override;

        protected:
            ////////////////////////////////////////////////////////////
            /// Member data
            ////////////////////////////////////////////////////////////
            float _radius;
    };
} //namespace graphics
} //namespace gn

#endif //__SPHERESHAPE_HPP__