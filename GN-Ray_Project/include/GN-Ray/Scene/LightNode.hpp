#ifndef __LIGHTNODE_HPP__
#define __LIGHTNODE_HPP__

#include "GN-Ray/Scene/AbstractNode.hpp"
#include "GN-Ray/Math/RigidBodyTransformation.hpp"
#include "GN-Ray/RayTracing/Ray.hpp"
#include "GN-Ray/Lights/Light.hpp"

namespace gn
{
namespace scene
{
    class LightNode : public AbstractNode
    {
        public:
            
            LightNode();

            virtual ~LightNode();

            inline ::gn::lights::Light* get_light() { return this->_light; }

            inline void set_light(::gn::lights::Light *light) { this->_light = light; }

            virtual bool accept(SceneVisitor &visitor) override;

            virtual bool light_me(const ::gn::math::Rbt &rbt, const ::gn::ray::Ray &ray);

        private:
            ////////////////////////////////////////////////////////////
            /// Member data
            ////////////////////////////////////////////////////////////
            ::gn::lights::Light *_light;
    };
} //namespace scene
} //namespace gn

#endif //__LIGHTNODE_HPP__