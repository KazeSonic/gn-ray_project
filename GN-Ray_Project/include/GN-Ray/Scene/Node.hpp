#ifndef __NODE_HPP__
#define __NODE_HPP__

#include <unordered_set>

#include "GN-Ray/Math.hpp"
#include "GN-Ray/Scene/AbstractNode.hpp"

namespace gn
{
namespace scene
{
    ////////////////////////////////////////////////////////////
    /// \brief A node is a transformation node. It contains a 
    /// rigid body transformation and can have children who will
    /// inherit of that transformation. It is not displayed on
    /// the screen.
    ////////////////////////////////////////////////////////////
    class Node : public AbstractNode
    {
        public:
            
            ////////////////////////////////////////////////////////////
            /// \brief Default constructor. No translation, no rotation.
            ////////////////////////////////////////////////////////////
            Node();
            
            ////////////////////////////////////////////////////////////
            /// \brief Default destructor.
            /// CAUTION : It will not delete the children.
            ////////////////////////////////////////////////////////////
            virtual ~Node();

            ////////////////////////////////////////////////////////////
            /// \brief Getters / Setters
            ////////////////////////////////////////////////////////////
            inline ::gn::math::Rbt& get_rbt() { return this->_rbt; }

            inline const ::gn::math::Rbt& get_rbt() const { return this->_rbt; }

            inline void set_rbt(const ::gn::math::Rbt &rbt) { this->_rbt = rbt; }

            ////////////////////////////////////////////////////////////
            /// \brief When a node accept a visitor it forwards it to its
            /// children.
            ////////////////////////////////////////////////////////////
            virtual bool accept(SceneVisitor &visitor) override;

            ////////////////////////////////////////////////////////////
            /// \brief Attach a child to this node.
            ////////////////////////////////////////////////////////////
            void add_child(AbstractNode *child);

            ////////////////////////////////////////////////////////////
            /// \brief Remove the given attached a child of this node.
            /// CAUTION : It will not delete it.
            ////////////////////////////////////////////////////////////
            void remove_child(AbstractNode *child);

            ////////////////////////////////////////////////////////////
            /// \brief Remove all children attached to this node witout
            /// deleting them.
            ////////////////////////////////////////////////////////////
            void clear_children();

        private:
            ////////////////////////////////////////////////////////////
            /// Member data
            ////////////////////////////////////////////////////////////
            std::unordered_set<AbstractNode*>   _children;  ///< Children nodes
            ::gn::math::Rbt                     _rbt;       ///< Position and rotation of the node
    };
} //namespace scene
} //namespace gn

#endif //__NODE_HPP__