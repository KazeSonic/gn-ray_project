#ifndef __SCENEGRAPH_HPP__
#define __SCENEGRAPH_HPP__

#include "GN-Ray/NonCopyable.hpp"
#include "Node.hpp"

namespace gn
{
namespace scene
{
    class SceneGraph : public NonCopyable
    {
        public:
            
            SceneGraph();

            virtual ~SceneGraph();
        
            inline Node& get_root() { return this->_root; }

            inline const Node& get_root() const { return this->_root; }

        protected:
            ////////////////////////////////////////////////////////////
            /// Member data
            ////////////////////////////////////////////////////////////
            Node _root; ///< Root of the scene (of the world also)
    };

} //namespace scene
} //namespace gn

#endif //__SCENEGRAPH_HPP__