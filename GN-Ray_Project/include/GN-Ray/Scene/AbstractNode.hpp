#ifndef __ABSTRACTNODE_HPP__
#define __ABSTRACTNODE_HPP__

#include "GN-Ray/NonCopyable.hpp"

namespace gn
{
    namespace scene
    {
        class SceneVisitor;
    } //namespace scene
} //namespace gn

namespace gn
{
namespace scene
{
    ////////////////////////////////////////////////////////////
    /// \brief Abstract scene node class.
    ////////////////////////////////////////////////////////////
    class AbstractNode : public NonCopyable
    {
        public:
            
            ////////////////////////////////////////////////////////////
            /// \brief Default constructor (nothing to do).
            ////////////////////////////////////////////////////////////
            AbstractNode() {}

            ////////////////////////////////////////////////////////////
            /// \brief Default destructor (nothing to do).
            ////////////////////////////////////////////////////////////
            virtual ~AbstractNode() {}

            ////////////////////////////////////////////////////////////
            /// \brief Overload equals operator. A node is equals to 
            /// another node if and only if they have the same address.
            ////////////////////////////////////////////////////////////
            bool operator == (const AbstractNode &other) const { return this == &other; }
            
            bool operator != (const AbstractNode &other) const { return this != &other; }

            ////////////////////////////////////////////////////////////
            /// \brief Pure virtual methode to accept a visitor.
            ////////////////////////////////////////////////////////////
            virtual bool accept(SceneVisitor &visitor) = 0;
    };
} //namespace scene
} //namespace gn

#endif //__ABSTRACTNODE_HPP__