#ifndef __SHAPENODE_HPP__
#define __SHAPENODE_HPP__

#include "GN-Ray/Scene/AbstractNode.hpp"
#include "GN-Ray/Math/RigidBodyTransformation.hpp"
#include "GN-Ray/RayTracing/Ray.hpp"
#include "GN-Ray/Graphics/Shape.hpp"

namespace gn
{
namespace scene
{
    ////////////////////////////////////////////////////////////
    /// \brief A shape node is a node with a shape and a material
    /// applied to it. 
    /// IMPORTANT : It does not do memory management for the shape
    /// or the material.
    ////////////////////////////////////////////////////////////
    class ShapeNode : public AbstractNode
    {
        public:
            
            ////////////////////////////////////////////////////////////
            /// \brief Default constructor.
            ////////////////////////////////////////////////////////////
            ShapeNode();

            ////////////////////////////////////////////////////////////
            /// \brief Default destructor. It will not delete the shape or
            /// the material.
            ////////////////////////////////////////////////////////////
            virtual ~ShapeNode();

            ////////////////////////////////////////////////////////////
            /// \brief Getters / Setters
            ////////////////////////////////////////////////////////////
            inline ::gn::graphics::Shape* get_shape() { return this->_shape; }

            inline void set_shape(::gn::graphics::Shape *shape) { this->_shape = shape; }

            ////////////////////////////////////////////////////////////
            /// \brief Override of the inherited accept methode.
            ////////////////////////////////////////////////////////////
            virtual bool accept(SceneVisitor &visitor) override;

            virtual float hit(const ::gn::math::Rbt &rbt, const ::gn::ray::Ray &ray);

            virtual bool get_normal_at_intersection(const ::gn::math::Rbt &rbt, const ::gn::math::Vector3 &intersection, ::gn::math::Vector3 &normal);

        private:
            ////////////////////////////////////////////////////////////
            /// Member data
            ////////////////////////////////////////////////////////////
            ::gn::graphics::Shape *_shape;
    };
} //namespace scene
} //namespace gn

#endif //__SHAPENODE_HPP__