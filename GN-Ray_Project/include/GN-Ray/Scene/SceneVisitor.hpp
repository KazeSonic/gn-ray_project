#ifndef __SCENEVISITOR_HPP__
#define __SCENEVISITOR_HPP__

#include <stack>
#include <vector>

#include "GN-Ray/NonCopyable.hpp"
#include "GN-Ray/Math.hpp"

#include "AbstractNode.hpp"
#include "Node.hpp"
#include "ShapeNode.hpp"
#include "LightNode.hpp"

namespace gn
{
namespace scene
{
    class SceneVisitor : public NonCopyable
    {
        public:
            
            SceneVisitor();

            virtual ~SceneVisitor();

            virtual bool visit(AbstractNode &node) { return false; }

            virtual bool visit(Node &node);

            virtual bool visit(ShapeNode &node);

            virtual bool visit(LightNode &node);


            virtual bool post_visit(AbstractNode &node) { return false; }

            virtual bool post_visit(Node &node);

            virtual bool post_visit(ShapeNode &node){ return true; }

            virtual bool post_visit(LightNode &node){ return true; }

        protected:

            void clear();
        
        protected:

            std::stack<::gn::math::Rbt> _transformation;
            std::vector<std::pair<::gn::math::Rbt, ShapeNode*>> _linearScene;
            std::vector<std::pair<::gn::math::Rbt, LightNode*>> _lights;
    };
} //namespace scene
} //namespace gn

#endif //__SCENEVISITOR_HPP__