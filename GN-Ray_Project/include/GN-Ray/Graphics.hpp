#ifndef __GRAPHICS_HPP__
#define __GRAPHICS_HPP__

#include <GN-Ray/Graphics/Color.hpp>
#include <GN-Ray/Graphics/Shape.hpp>
#include <GN-Ray/Graphics/Vertex.hpp>

//shape example
#include <GN-Ray/Graphics/BasicShape/SphereShape.hpp>

#endif //__GRAPHICS_HPP__